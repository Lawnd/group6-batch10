/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {
      colors: {
        maizeYellow: "var(--maize-yellow)",
        cornflowerBlue: "var(--cornflower-blue)",
        periwinkleBlue: "var(--periwinkle-blue)",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      animation: {
        shake: "shake 0.1s",
      },
      backgroundImage: {
        "landing-page": "url('/src/assets/images/bg-landing-page.jpeg')",
      },
    },
  },
  plugins: [],
};
