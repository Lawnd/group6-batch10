import GojekIcon from "../assets/images/ico-gopay.svg";
import OvoIcon from "../assets/images/ico-ovo.svg";
import DanaIcon from "../assets/images/ico-dana.svg";
import MandiriIcon from "../assets/images/ico-mandiri.svg";
import BcaIcon from "../assets/images/ico-bca.svg";
import BniIcon from "../assets/images/ico-bni.svg";

import Class1 from "../assets/images/class-1.jpg";
import Class2 from "../assets/images/class-2.jpeg";
import Class3 from "../assets/images/class-3.jpeg";
import Class4 from "../assets/images/class-4.jpeg";

import TypeClass1 from "../assets/images/type-class1.jpeg";
import TypeClass2 from "../assets/images/type-class2.jpeg";
import TypeClass3 from "../assets/images/type-class3.jpeg";
import TypeClass4 from "../assets/images/type-class4.jpeg";
import TypeClass5 from "../assets/images/type-class5.jpeg";
import TypeClass6 from "../assets/images/type-class6.jpeg";

import FavClass1 from "../assets/images/fav-class1.jpeg";
import FavClass2 from "../assets/images/fav-class2.jpeg";
import FavClass3 from "../assets/images/fav-class3.jpeg";
import FavClass4 from "../assets/images/fav-class4.jpeg";
import FavClass5 from "../assets/images/fav-class5.jpeg";
import FavClass6 from "../assets/images/fav-class6.jpeg";

const methodPaymentData = [
  {
    id: 1,
    ico: GojekIcon,
    name: "Gopay",
    isSelected: false,
  },
  {
    id: 2,
    ico: OvoIcon,
    name: "OVO",
    isSelected: false,
  },
  {
    id: 3,
    ico: DanaIcon,
    name: "DANA",
    isSelected: false,
  },
  {
    id: 4,
    ico: MandiriIcon,
    name: "Mandiri",
    isSelected: false,
  },
  {
    id: 5,
    ico: BcaIcon,
    name: "BCA",
    isSelected: false,
  },
  {
    ico: BniIcon,
    name: "BNI",
    isSelected: false,
  },
];

const typeClassData = [
  {
    ico: TypeClass1,
    name: "Drum",
  },
  {
    ico: TypeClass2,
    name: "Piano",
  },
  {
    ico: TypeClass3,
    name: "Gitar",
  },
  {
    ico: TypeClass3,
    name: "Bass",
  },
  {
    ico: TypeClass4,
    name: "Biola",
  },
  {
    ico: TypeClass5,
    name: "Menyanyi",
  },
  {
    ico: TypeClass6,
    name: "Flute",
  },
  {
    ico: TypeClass6,
    name: "Saxophone",
  },
];

const favClassData = [
  {
    ico: FavClass1,
    type: "Drum",
    name: "Kursus Drummer Special Coach (Eno Netral)",
    price: 8500000,
  },
  {
    ico: FavClass2,
    type: "Gitar",
    name: "[Beginner] Guitar class for kids ",
    price: 1600000,
  },
  {
    ico: FavClass3,
    type: "Biola",
    name: "Biola Mid-Level Course",
    price: 3000000,
  },
  {
    ico: FavClass4,
    type: "Drum",
    name: "Drummer for kids (Level Basic/1)",
    price: 2200000,
  },
  {
    ico: FavClass5,
    type: "Piano",
    name: "Kursu Piano : From Zero to Pro (Full Package)",
    price: 11650000,
  },
  {
    ico: FavClass6,
    type: "Saxophone",
    name: "Expert Level Saxophone",
    price: 7350000,
  },
];

const offeringData = [
  {
    total: "500+",
    desc: "Lebih dari sekedar kelas biasa yang bisa mengeluarkan bakat kalian",
  },
  {
    total: "50+",
    desc: "Lulusan yang menjadi musisi ternama dengan skill memukau",
  },
  {
    total: "10+",
    desc: "Coach Special kolaborasi dengan musisi terkenal",
  },
];

let checkoutDataDummy = [
  {
    id: "1e41151a-be6a-491d-a6c5-cd9c3012e065",
    img: Class1,
    type: "Drum",
    name: "Kursus Drummer Special Coach (Eno Netral)",
    date: new Date("Senin, 25 Juli 2022"),
    price: 8500000,
    isSelected: false,
  },
  {
    id: "d251cc24-12e2-4483-9efb-73235e171617",
    img: Class2,
    type: "Biola",
    name: "Biola Mid-Level Course",
    date: new Date("Sabtu, 23 Juli 2022"),
    price: 3000000,
    isSelected: false,
  },
  {
    id: "457aae76-607d-44c5-961e-118c5dcb34d2",
    img: Class3,
    type: "Drum",
    name: "Expert Level Drummer Lessons",
    date: new Date("Senin, 25 Juli 2022"),
    price: 5450000,
    isSelected: false,
  },
  {
    id: "5f21651c-b03e-4885-a04b-7e93e468c880",
    img: Class4,
    type: "Saxophone",
    name: "Expert Level Saxophone",
    date: new Date("Rabu, 27 Juli 2022"),
    price: 7350000,
    isSelected: false,
  },
];

export {
  methodPaymentData,
  checkoutDataDummy,
  typeClassData,
  offeringData,
  favClassData,
};
