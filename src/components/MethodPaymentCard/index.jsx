import PropTypes from "prop-types";
import { Typography, Button } from "@mui/material";
import "../../styles/custom.css";

const MethodPaymentCard = (props) => {
  return (
    <Button
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "start",
        width: "20.375rem",
        height: "2.7rem",
        margin: "0.5rem",
        backgroundColor: props.isSelected ? "rgba(111,113,220, 0.3)" : "white",
        "&:hover": {
          backgroundColor: "rgba(111,113,220, 0.3)",
        },
      }}
      onClick={() => props.handleSelectedPayment(props.id)}>
      <img
        className="logo-method"
        src={`data:image/jpeg;base64,${props.ico}`}
      />

      <Typography
        sx={{
          paddingX: "1rem",
          fontSize: "0.8rem",
          color: props.isSelected ? "white" : "black",
        }}>
        {props.name}
      </Typography>
    </Button>
  );
};


// Menambahkan PropTypes
MethodPaymentCard.propTypes = {
  isSelected: PropTypes.bool,
  handleSelectedPayment: PropTypes.func,
  id: PropTypes.string,
  ico: PropTypes.string,
  name: PropTypes.string,
};

export default MethodPaymentCard;
