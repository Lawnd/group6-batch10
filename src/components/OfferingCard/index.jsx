import { Box, Typography } from "@mui/material";

const OfferingCard = (props) => {
  return (
    <Box
      sx={{
        display: "flex",
        marginX: {
          md: 3,
          xl: 5,
        },
        justifyContent: "space-between",
        flexDirection: "column",
        alignItems: "center",
        borderRadius: "0.75rem",
        backgroundColor: "white",
        paddingX: {
          xs: 3,
          sm: 3,
        },
        paddingTop: "2%",
        width: {
          xs: "95%",
          sm: "45%",
          md: "30%",
          xl: "20%",
        },
        height: {
          xs: "10rem",
          sm: "11.5rem",
          md: "13rem",
          lg: "15rem",
        },
        marginY: 2,
      }}>
      <Typography
        sx={{
          color: "hsl(239, 61%, 65%)",
          fontSize: {
            xs: "3rem",
            md: "3.5rem",
            xl: "3.75rem",
          },
          height: "40%",
          fontWeight: "bold",
        }}>
        {props?.total}
      </Typography>
      <Typography
        sx={{
          fontSize: {
            xs: "1rem",
            lg: "1.125rem",
          },
          fontWeight: "bold",
          textAlign: "center",
          height: "50%",
        }}>
        {props?.desc}
      </Typography>
    </Box>
  );
};

export default OfferingCard;

// <div className='my-2 sm:my-0 sm:w-[27%] lg:w-1/5 sm:h-52 lg:h-60 bg-white flex flex-col items-center rounded-xl py-9 lg:py-11 mx-3'>
//   <h2 className='text-cornflowerBlue sm:text-5xl lg:text-6xl font-bold sm:h-1/2 lg:h-2/3'>
//     {props?.total}
//   </h2>
//   <p className='text-center px-5 font-medium lg:text-lg'>{props?.desc}</p>
// </div>
