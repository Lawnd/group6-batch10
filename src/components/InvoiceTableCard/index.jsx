import React from 'react';
import PropTypes from 'prop-types';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import Button from '@mui/material/Button';
import { dateFormat, priceIDFormat } from '../../utils/formatter';
import { useNavigate } from 'react-router-dom';

const InvoiceTableCard = (props) => {
  const { index, id, no, invoice, date, price, totalcourse } = props.item; 
  const navigate = useNavigate();

  const handleDetail = () => {
    navigate(`/detailinvoice/${id}`);
  };

  return (
    <TableRow key={id} style={{ backgroundColor: index % 2 === 0 ? "white" : "#FCF4DB" }}>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{no}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{invoice}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{dateFormat(date)}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{totalcourse}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{priceIDFormat(price)}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>
        <Button
          sx={{
            backgroundColor: "hsl(239, 61%, 65%)",
            color: "white",
            border: 1,
            paddingX: 2,
            paddingY: 0.5,
            width: "100%",
            height: 40,
            display: 'flex',
            justifyContent: 'center',
            "&:hover": {
              backgroundColor: "hsl(239, 61%, 75%)",
              border: 1,
              borderColor: "hsl(239, 61%, 65%)",
              color: "hsl(239, 61%, 45%)",
            },
          }}
          onClick={handleDetail}
          
        >
          Rincian
        </Button>
      </TableCell>
    </TableRow>
  );
};

InvoiceTableCard.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.string,
    no: PropTypes.number,
    invoice: PropTypes.string,
    date: PropTypes.string,
    totalcourse: PropTypes.number,
    price: PropTypes.number,
  }),
  index: PropTypes.number,
};

export default InvoiceTableCard;
