import { Box, Typography, Checkbox, Button } from "@mui/material";
import { MdDeleteForever } from "react-icons/md";
import { dateFormat, priceIDFormat } from "../../utils/formatter";
import PropTypes from "prop-types";

const CheckoutCard = (props) => {
  const { id, category, title, date, price, image, isSelected } = props.items;

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: {
          xs: "18rem",
          sm: "10rem",
          lg: "8rem",
          xl: "9rem",
        },
        borderBottom: "0.05rem",
        borderBottomColor: "black",
        borderStyle: "solid",
        marginBottom: {
          xs: "0.01rem",
          sm: "0.3rem",
        },
        width: "100%",
      }}>
      <Box
        sx={{
          width: "3.5rem",
          height: {
            xs: "3rem",
            md: "100%",
          },
          display: "flex",
          alignItems: "center",
        }}>
        <Checkbox
          checked={props?.isSelected}
          onChange={() => props?.handleSelect(id)}
        />
      </Box>

      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: {
            xs: "center",
            xl: "start",
          },
          flexDirection: {
            xs: "column",
            sm: "row",
          },
          width: {
            xs: "17.5rem",
            sm: "50rem",
            lg: "55rem",
            xl: "57.5rem",
          },
          height: {
            xs: "100%",
            xl: "8.375rem",
          },
        }}>
        <Box
          component='img'
          src={`data:image/jpeg;base64,${image}`}
          sx={{
            width: {
              xs: "100%",
              sm: "11rem",
              xl: "15rem",
            },
            height: {
              xs: "8rem",
              sm: "6rem",
              xl: "7.5rem",
            },
            borderRadius: "1rem",
            marginRight: {
              xs: 0,
              xl: "1.5rem",
            },
            marginBottom: {
              xs: "0.3rem",
              xl: 0,
            },
            objectFit: "cover",
            objectPosition: "top",
          }}
        />
        <Box
          sx={{
            width: "100%",
            paddingLeft: {
              xs: 0,
              sm: "1.2rem",
            },
          }}>
          <Typography
            sx={{
              color: "#828282",
              fontSize: "1rem",
            }}>
            {category}
          </Typography>
          <Typography
            sx={{
              fontWeight: "bold",
              fontSize: {
                xs: "1rem",
                sm: "1.2rem",
                xl: "1.5rem",
              },
            }}>
            {title}
          </Typography>
          <Typography sx={{ color: "#4F4F4F", fontSize: "1rem" }}>
            {`${dateFormat(date)}`}
          </Typography>
          <Typography
            sx={{
              color: "#5D5FEF",
              fontWeight: "bold",
              fontSize: {
                xs: "1rem",
                sm: "1.1rem",
                xl: "1.25rem",
              },
            }}>
            {priceIDFormat(price ? price : 0)}
          </Typography>
        </Box>
      </Box>
      <Button
        onClick={() => props?.removeCheckout(id)}
        sx={{
          display: "flex",

          width: {
            xs: "5rem",
            sm: "12.5rem",
            md: "9rem",
            lg: "8.5rem",
            xl: "8rem",
          },
          marginRight: {
            xs: 0,
            sm: "0.5rem",
            md: "1.5rem",
            lg: "3rem",
          },
        }}>
        <MdDeleteForever className='fill-red-500 w-9 h-9 sm:w-7 sm:h-7' />
        <Typography
          sx={{
            paddingLeft: "0.5rem",
            fontSize: "1rem",
            display: {
              xs: "none",
              sm: "block",
            },
          }}>
          Delete
        </Typography>
      </Button>
    </Box>
  );
};

CheckoutCard.propTypes = {
  items: PropTypes.shape({
    id: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }).isRequired,
  isSelected: PropTypes.bool.isRequired,
  handleSelect: PropTypes.func.isRequired,
  removeCheckout: PropTypes.func.isRequired,
};

export default CheckoutCard;
