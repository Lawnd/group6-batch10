import * as React from "react";
import {
  Box,
  Grid,
  Card,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import { priceIDFormat } from "../../utils/formatter";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

const FavClassCard = (props) => {
  const { id, source, type, name, price } = props?.items;
  const navigate = useNavigate();

  const handleCardClick = () => {
    navigate(`/detailclass/${id}`);
  };

  return (
    <Grid
      item
      xs={5.5}
      md={4.5}
      lg={4}
      xl={4}
      display='flex'
      justifyContent='center'
      alignItems='center'>
      <div onClick={handleCardClick} style={{ cursor: "pointer" }}>
        <Card
          sx={{
            width: {
              xs: 150,
              sm: 200,
              md: 300,
              xl: 400,
            },
            height: {
              xl: "27rem",
            },
            border: 0,
            boxShadow: 0,
          }}>
          <CardMedia
            sx={{
              height: {
                xs: 100,
                sm: 200,
                md: 300,
                lg: 220,
                xl: 250,
              },
              width: "100%",
              borderRadius: "3%",
              border: 1,
              borderColor: "gray",
            }}
            image={`data:image/jpeg;base64,${source}`}
          />
          <CardContent>
            <Box>
              <Typography variant='p' color='text.secondary'>
                {type}
              </Typography>
              <Typography
                variant='h6'
                component='div'
                fontWeight='bold'
                sx={{
                  height: {
                    xs: "7.5rem",
                    sm: "4rem",
                  },
                  fontSize: {
                    xs: "1.1rem",
                    lg: "1.3rem",
                  },
                }}
                marginBottom={3}>
                {name}
              </Typography>
            </Box>

            <Typography
              variant='h6'
              component='div'
              sx={{
                fontWeight: "bold",
                fontSize: {
                  xs: "1rem",
                  sm: "1.3rem",
                },
              }}
              className='text-cornflowerBlue'>
              {priceIDFormat(price)}
            </Typography>
          </CardContent>
        </Card>
      </div>
    </Grid>
  );
};

FavClassCard.propTypes = {
  items: PropTypes.shape({
    id: PropTypes.string.isRequired,
    source: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }).isRequired,
};

export default FavClassCard;
