import { Grid, Box, Typography } from "@mui/material";
import { dateFormat } from "../../utils/formatter";
import PropTypes from "prop-types"; // Import PropTypes correctly

const MyClassCard = (props) => {
  const { img, type, name, date } = props.item;

  return (
    <div>
      <Grid
        container
        sx={{
          display: "flex",
          gridGap: "16px",
          padding: 1,
          justifyContent: "center",
          alignItems: "center",
          paddingX: 5,
        }}
      >
        <Box
          sx={{
            borderBottom: (theme) => `1px solid rgba(0, 0, 0, 0.2)`,
            width: "100%",
            display: "flex",
            paddingY: 2,
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              textAlign: "center",
              padding: "16px",
              width: "200px",
              height: "150px",
              overflow: "hidden",
            }}
          >
            <img
              src={img}
              style={{
                width: "100%",
                height: "100%",
                objectFit: "cover",
                borderRadius: "10%",
              }}
            />
          </Box>

          <Box
            sx={{
              padding: "16px",
              width: "700px",
              alignItems: "center",
            }}
          >
            <Typography variant="p" color="text.secondary">
              {type}
            </Typography>

            <Typography
              variant="h6"
              component="div"
              fontWeight="bold"
              sx={{ fontSize: { xs: 16, sm: 18, md: 22 } }}
            >
              {name}
            </Typography>

            <Typography
              variant="h6"
              component="div"
              className="text-cornflowerBlue"
              marginTop={1}
              sx={{ fontSize: { xs: 16, sm: 18, md: 22 } }}
            >
              {`Jadwal: ${dateFormat(date)}`}
            </Typography>
          </Box>
        </Box>
      </Grid>
    </div>
  );
};

MyClassCard.propTypes = {
  item: PropTypes.shape({
    img: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    date: PropTypes.string,
  }),
};

export default MyClassCard;
