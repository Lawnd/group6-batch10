import React from 'react';
import PropTypes from 'prop-types';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import { dateFormat, priceIDFormat } from '../../utils/formatter';

const InvoiceDetailTableRow = (props) => {

    const { index, id, no, namecourse, category, schedule, price,  } = props.item; 

  return (
    <TableRow key={index} style={{ backgroundColor: index % 2 === 0 ? "white" : "#FCF4DB" }}>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{no}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{namecourse}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{category}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{dateFormat(schedule)}</TableCell>
      <TableCell align="center" sx={{ fontSize: { xs: 12, md: 16 } }}>{priceIDFormat(price)}</TableCell>
    </TableRow>
  );
};

InvoiceDetailTableRow.propTypes = {
  index: PropTypes.number,
  item: PropTypes.shape({
    id: PropTypes.string,
    no: PropTypes.number,
    namecourse: PropTypes.string,
    category: PropTypes.string,
    schedule: PropTypes.string,
    price: PropTypes.number,
  }),
};

export default InvoiceDetailTableRow;
