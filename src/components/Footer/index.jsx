import { Box, Typography } from "@mui/material";
import { MdCall, MdOutlineMail } from "react-icons/md";
import { AiOutlineInstagram, AiOutlineYoutube } from "react-icons/ai";
import { LiaTelegramPlane } from "react-icons/lia";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <Box
      sx={{
        width: "100%",
        paddingX: {
          md: "2rem",
          lg: "4rem",
          xl: "7rem",
        },
        paddingTop: {
          xs: "1rem",
          sm: "1.5rem",
          md: "2rem",
          lg: "3rem",
        },
        paddingBottom: {
          xs: "0.5rem",
          md: "1rem",
        },
        display: "flex",
        flexDirection: {
          xs: "column",
          sm: "row",
        },
        alignItems: {
          xs: "center",
          sm: "start",
        },
        justifyContent: {
          xs: "space-between",
          sm: "space-around",
        },
        backgroundColor: "hsl(45.2, 86.5%, 62.4%)",
      }}>
      <Box
        sx={{
          width: {
            xs: "85%",
            sm: "30%",
            md: "40%",
          },
          paddingBottom: "1rem",
        }}>
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.2rem",
            paddingBottom: "0.5rem",
          }}>
          Tentang
        </Typography>
        <Typography
          sx={{
            textAlign: "justify",
          }}>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
          ab illo inventore veritatis et quasi architecto beatae vitae dicta
          sunt explicabo.
        </Typography>
      </Box>
      <Box
        sx={{
          width: {
            xs: "85%",
            sm: "27%",
            md: "23.5%",
            lg: "20%",
          },

          paddingRight: {
            xs: "1.5rem",
            sm: 0,
          },
          paddingBottom: "1rem",
        }}>
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.2rem",
            paddingBottom: "0.5rem",
          }}>
          Produk
        </Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            paddingX: {
              xs: "4rem",
              sm: "1rem",
              md: "2.4rem",
            },
          }}>
          <ul className='list-disc'>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Biola</NavLink>
            </li>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Gitar</NavLink>
            </li>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Drum</NavLink>
            </li>
          </ul>
          <ul className='list-disc'>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Menyanyi</NavLink>
            </li>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Piano</NavLink>
            </li>
            <li className='footer hover:text-blue-500'>
              <NavLink to='/detailclass'>Biola</NavLink>
            </li>
          </ul>
        </Box>
      </Box>
      <Box
        sx={{
          width: {
            xs: "85%",
            sm: "35%",
            md: "30%",
            xl: "25%",
            paddingBottom: "1rem",
          },
        }}>
        <Box>
          <Typography
            sx={{
              fontWeight: "bold",
              fontSize: "1.2rem",
              paddingBottom: "0.5rem",
            }}>
            Alamat
          </Typography>
          <Typography>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque.
          </Typography>
        </Box>
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.2rem",
            paddingTop: "1rem",
          }}>
          Kontak
        </Typography>
        <Box sx={{ display: "flex", flexWrap: "wrap" }}>
          <MdCall className='icon-footer' />
          <AiOutlineInstagram className='icon-footer' />
          <AiOutlineYoutube className='icon-footer' />
          <LiaTelegramPlane className='icon-footer' />
          <MdOutlineMail className='icon-footer' />
        </Box>
      </Box>
    </Box>
  );
};

export default Footer;
