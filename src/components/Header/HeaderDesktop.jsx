import { Box, Typography } from "@mui/material";
import { Button } from "@mui/material";
import { AiFillApple } from "react-icons/ai";
import { BiSolidCart, BiSolidUser } from "react-icons/bi";
import { RxExit } from "react-icons/rx";
import { NavLink } from "react-router-dom";
import { FaHouseUser } from "react-icons/fa";

const HeaderDesktop = (props) => {
  return (
    <Box
      sx={{
        backgroundColor: "#F2C94C",
        width: "100%",
        height: "6rem",
        display: {
          xs: "none",
          sm: "flex",
        },
        alignItems: "center",
        justifyContent: "space-between",
        paddingX: {
          xs: "10px",
          lg: "3rem",
          xl: "5rem",
        },
      }}>
      <NavLink to='/'>
        <Button
          sx={{
            display: "flex",
            justifyItems: "center",
            color: "black",
            height: "3rem",
          }}>
          <AiFillApple className='w-7 h-7 !fill-black' />
          <Typography
            sx={{
              fontSize: "1.125rem",
              fontWeight: "bold",
              paddingLeft: "0.25rem",
            }}>
            MUSIC
          </Typography>
        </Button>
      </NavLink>
      {props?.userData.email ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "end",
            alignItems: "center",
            height: "66%",
            width: "75%",
          }}>
          <NavLink to='checkoutpage'>
            <Button
              sx={{
                marginX: "1rem",
                minWidth: "2.5rem",
                minHeight: "2.5rem",
                borderRadius: "0.25rem",
                color: "black",
              }}>
              <BiSolidCart className='w-10 h-10 p-2 fill-black' />
            </Button>
          </NavLink>
          <NavLink to='myclass'>
            <Button
              sx={{
                width: "6rem",
                height: "3rem",
                color: "black",
                fontWeight: "bold",
                marginX: "1.25rem",
              }}>
              Kelasku
            </Button>
          </NavLink>
          <NavLink to='invoice'>
            <Button
              sx={{
                width: "6rem",
                height: "3rem",
                color: "black",
                fontWeight: "bold",
                marginX: "1.25rem",
              }}>
              Pembelian
            </Button>
          </NavLink>
          <Box
            sx={{
              height: "33%",
              backgroundColor: "black",
              display: "block",
              width: "0.125rem",
              marginX: "1.25rem",
            }}
          />

          {props?.userData?.role == "True" && (
            <NavLink to='dashboard'>
              <Button
                sx={{
                  width: "6rem",
                  height: "3rem",
                  color: "black",
                  fontWeight: "bold",
                  marginX: "1.25rem",
                }}>
                <FaHouseUser className='w-10 h-10 p-2 fill-black' />
              </Button>
            </NavLink>
          )}

          <Button
            onClick={props?.handleLogout}
            sx={{
              width: "6rem",
              height: "3rem",
              color: "black",
              fontWeight: "bold",
              marginX: "1.25rem",
            }}>
            <RxExit className='w-10 h-10 p-2 fill-black' />
          </Button>
        </Box>
      ) : (
        <Box
          sx={{
            display: "flex",
            justifyItems: "center",
          }}>
          <NavLink to='/register'>
            <Button
              sx={{
                width: "11rem",
                height: "3rem",
                color: "black",
                fontWeight: "bold",
              }}>
              Daftar Sekarang
            </Button>
          </NavLink>
          <NavLink to='/login'>
            <Button
              sx={{
                backgroundColor: "#5D5FEF",
                "&:hover": {
                  backgroundColor: "#8D8FF3",
                },
                width: "5rem",
                height: "3rem",
                color: "white",
                fontWeight: "bold",
                marginX: "1.25rem",
                marginLeft: "2.5rem",
              }}>
              Masuk
            </Button>
          </NavLink>
        </Box>
      )}
    </Box>
  );
};

export default HeaderDesktop;
