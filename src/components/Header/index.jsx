import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import HeaderDesktop from "./HeaderDesktop";
import {
  Box,
  Typography,
  Menu,
  MenuItem,
  IconButton,
  Grid,
} from "@mui/material";
import ListIcon from "@mui/icons-material/List";
import AppleIcon from "@mui/icons-material/Apple";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../hooks";
import { FiLogIn, FiLogOut } from "react-icons/fi";
import PersonIcon from "@mui/icons-material/Person";

const pagesTitle = ["Kelasku", "Pembelian", "Checkout"];
const pagesRoute = ["myclass", "invoice", "checkoutpage"];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const { userData, logout } = useAuth();

  const navigate = useNavigate();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };
  const handleLogout = () => {
    try {
      logout();
      navigate("/");
    } catch (e) {
      throw e;
    }
  };

  return (
    <>
      <HeaderDesktop userData={userData} handleLogout={handleLogout} />
      {
        <Grid
          container
          sx={{
            display: { xs: "flex", sm: "none" },
            backgroundColor: "#F2C94C",
            width: "100%",
            height: "4rem",
            alignContent: "center",
          }}>
          <Box sx={{ flexGrow: 0.5, display: { xs: "flex", sm: "none" } }}>
            <IconButton
              size='large'
              aria-label='account of current user'
              aria-controls='menu-appbar'
              aria-haspopup='true'
              onClick={handleOpenNavMenu}
              color='inherit'>
              <ListIcon />
            </IconButton>
            <Menu
              id='menu-appbar'
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
              PaperProps={{
                style: {
                  backgroundColor: "white",
                },
              }}>
              {pagesTitle.map((page, index) => (
                <MenuItem
                  key={page}
                  onClick={() => {
                    handleCloseNavMenu();
                    navigate(`/${pagesRoute[index]}`);
                  }}>
                  <Typography textAlign='center'>{page}</Typography>
                </MenuItem>
              ))}
              {userData?.role == "True" && (
                <MenuItem
                  onClick={() => {
                    handleCloseNavMenu();
                    navigate(`/dashboard`);
                  }}>
                  <Typography textAlign='center'>{"Dashboard"}</Typography>
                </MenuItem>
              )}
            </Menu>
          </Box>
          <Box
            sx={{
              flexGrow: 0.45,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <AppleIcon sx={{ display: { xs: "flex", sm: "none" }, mr: 1 }} />
            <Typography
              variant='h5'
              noWrap
              component='a'
              href='/'
              sx={{
                mr: 2,
                display: { xs: "flex", sm: "none" },
                flexGrow: 1,
                fontFamily: "monospace",
                fontWeight: 700,
                color: "inherit",
                textDecoration: "none",
                fontSize: { xs: "1.5rem" },
              }}>
              MUSIC
            </Typography>
          </Box>
          <Box
            sx={{
              justifyContent: "center",
              alignItems: "flex-end",
            }}>
            {userData?.email ? (
              <IconButton
                size='large'
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                color='inherit'
                onClick={() => {
                  handleLogout();
                }}>
                <FiLogOut />
              </IconButton>
            ) : (
              <IconButton
                size='large'
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                color='inherit'
                onClick={() => {
                  navigate(`/login`);
                }}>
                <FiLogIn />
              </IconButton>
            )}
          </Box>
        </Grid>
      }
      <main>
        <Outlet />
      </main>
    </>
  );
};

export default Header;
