import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography } from '@mui/material';

const ClassDescriptionBox = (props) => {
  const { src, title, description } = props;

  return (
    <Box>
      <Box
        component='img'
        src={src}
        sx={{
          width: '100%',
          height: '17rem',
          objectFit: 'cover',
        }}
      />

      <Box
        sx={{
          width: {
            xs: '80%',
            sm: '90%',
            xl: '80%',
          },
          display: 'flex',
          justifyContent: 'center',
          marginX: 'auto',
          flexDirection: 'column',
          height: {
            xs: '24rem',
            sm: '15rem',
          },
        }}>
        <Typography
          sx={{
            fontWeight: 'bold',
            fontSize: {
              xs: '1.2rem',
              sm: '1.45rem',
              xl: '1.65rem',
            },
            paddingBottom: '1rem',
          }}>
          {title}
        </Typography>
        <Typography
          sx={{
            fontSize: {
              xs: '1rem',
              sm: '1.1rem',
              xl: '1.25rem',
            },
          }}>
          {description}
        </Typography>
      </Box>
    </Box>
  );
};

ClassDescriptionBox.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default ClassDescriptionBox;
