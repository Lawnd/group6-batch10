import { useState } from "react";
import { TextField } from "@mui/material";

const Form = (props) => {
  const [isFocus, setFocus] = useState(false);

  const handleChange = (e) => {
    if (props.handleChange) props.handleChange(e, props.id);
  };

  return (
    <TextField
      id={props?.id}
      label={isFocus ? props.label : props.name}
      placeholder={props?.name}
      onChange={handleChange}
      onFocus={(e) => setFocus(true)}
      onBlur={(e) => setFocus(false)}
      className='w-full !my-3'
      type={props?.label === "Password" ? "password" : "text"}
      inputProps={{
        maxLength: props?.maxLength,
        style: {
          height: "1rem",
        },
      }}
    />
  );
};

export default Form;
