import React, { useEffect, useState } from "react";

function Notification(props) {
  const { isShow, msg, type } = props;
  const [open, setOpen] = useState(isShow);

  useEffect(() => {
    if (isShow) {
      setOpen(true);

      const timeoutId = setTimeout(() => {
        setOpen(false);
      }, 2500);

      return () => {
        clearTimeout(timeoutId);
      };
    }
  }, [isShow]);

  const notificationStyle = {
    backgroundColor: type === "success" ? "#FFF" : "#FFF", 
    color: type === "success" ? "#4CAF50" : "#f44336", 
    display: isShow ? "flex" : "none",
    alignItems: "center",
    justifyContent: "center", 
    padding: "8px",
    border: `2px solid ${type === "success" ? "#4CAF50" : "#f44336"}`,
    borderRadius: "7px",
    width: "auto",
    position: "fixed",
    top: "80px",
    left: "50%",
    transform: "translateX(-50%)",
    zIndex: 9999,
  };
  
  

  return (
    <div
      style={notificationStyle}
    >
      <p style={{ textAlign: "center" }}>{msg}</p>
    </div>
  );
  
}

export default Notification;


export const SuccessNotification = (setNotification, message, duration = 2500) => {
    setNotification({
      isShow: true,
      msg: message,
      type: "success",
    });
  
    // Setelah duration (default 2500ms), ubah isShow menjadi false
    setTimeout(() => {
      setNotification((prevNotification) => ({
        ...prevNotification,
        isShow: false,
      }));
    }, duration);
  };
  
  export const ErrorNotification = (setNotification, message, duration = 2500) => {
    setNotification({
      isShow: true,
      msg: message,
      type: "error",
    });
  
    // Setelah duration (default 2500ms), ubah isShow menjadi false
    setTimeout(() => {
      setNotification((prevNotification) => ({
        ...prevNotification,
        isShow: false,
      }));
    }, duration);
  };
  
