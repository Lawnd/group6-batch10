import { useEffect } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";

const Message = (props) => {
  useEffect(() => {
    const timeId = setTimeout(
      () =>
        props?.setMessage({
          isShow: false,
          msg: "",
        }),
      2500
    );
    return () => clearTimeout(timeId);
  }, []);

  return (
    <div className='w-4/5 sm:w-80 h-10 sm:h-16 bg-red-200 flex justify-center items-center rounded mx-auto border-2 border-red-600 animate-shake z-50 absolute top-2 sm:top-5 left-0 right-0 text-center'    >
      <AiOutlineCloseCircle className='w-6 h-6 fill-red-600' />
      <p className='pl-2 text-red-700'>
        {props?.msg === "email"
          ? "Email Anda Tidak Valid"
          : props?.msg === "password"
          ? "Password Anda Tidak Valid"
          : props?.msg === "name"
          ? "Nama Anda Tidak Valid"
          : props?.apiErrorMsg || ""}
      </p>
    </div>
  );
};

export default Message;
