import { CardMedia, Typography, Grid, CardContent, Card } from "@mui/material";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

const TypeClassCard = (props) => {
  const { source, name, id } = props?.items; 
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/menuclass/${id}`); 
  };

  return (
    <Grid
      item
      xs={5}
      sm={4}
      md={3}
      lg={2.5}
      display='flex'
      width={"100%"}
      sx={{
        alignItems: "center",
        justifyContent: "center",
      }}>
      <Card
        display='flex'
        onClick={handleClick}
        sx={{
          border: 0,
          boxShadow: 0,
          marginX: 2,
          width: {
            xs: 100,
            sm: 150,
            md: 280,
            lg: 200,
          },
          backgroundColor: "transparent",
        }}>
        <CardMedia
          sx={{
            height: {
              xs: 60,
              sm: 100,
              md: 75,
              lg: 90,
            },
            width: {
              xs: 100,
              sm: 150,
              md: 130,
              lg: 150,
            },
            backgroundSize: "fit",
            backgroundPosition: "center",
            marginX: "auto",
            borderRadius: "0.5rem",
          }}
          image={source}
        />
        <CardContent sx={{ paddingTop: 1 }}>
          <Typography
            textAlign='center'
            sx={{
              fontWeight: "bold",
              fontSize: {
                xs: 12,
                sm: 14,
                md: 16,
                lg: 18,
                xl: 20,
              },
            }}>
            {name}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

TypeClassCard.propTypes = {
  items: PropTypes.shape({
    source: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }).isRequired,
};

export default TypeClassCard;
