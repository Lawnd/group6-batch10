// AuthContext.js
import React, { createContext, useState, useEffect } from "react";
import jwtDecode from "jwt-decode";

export const AuthContext = createContext(null);

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(";");

  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function decodeJWT(token) {
  return jwtDecode(token);
}

export const AuthProvider = ({ children }) => {
  const [payload, setPayload] = useState(null);
  const [userData, setUserData] = useState({
    email: null,
    role: null,
    id: null,
  });

  useEffect(() => {
    const payloadRaw = getCookie("payload");
    if (payloadRaw) {
      const jsonPayload = JSON.parse(payloadRaw);
      setPayload(jsonPayload);

      const decodedPayload = decodeJWT(payloadRaw);
      setUserData({
        email: decodedPayload?.email,
        role: decodedPayload?.role,
        id: decodedPayload?.Id,
      });
    }
  }, []);

  const login = (token) => {
    setCookie("payload", JSON.stringify(token), 1);
    setPayload(token);

    const decodedPayload = decodeJWT(token);
    setUserData({
      email: decodedPayload?.email,
      role: decodedPayload?.role,
      id: decodedPayload?.Id,
    });
  };

  const logout = () => {
    setCookie("payload", null, 0);
    setPayload(null);
    setUserData({
      email: null,
      role: null,
      id: null,
    });
  };

  return (
    <AuthContext.Provider
      value={{
        payload,
        userData,
        login,
        logout,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
