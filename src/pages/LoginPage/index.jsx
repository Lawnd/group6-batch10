import React, { useState } from "react";
import { Form, Message } from "../../components";
import { Box, Button, Typography } from "@mui/material";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth";

const LoginPage = () => {
  const navigate = useNavigate();
  const [apiErrorMsg, setApiErrorMsg] = useState(""); // message error from API
  const { login, isLoggedIn } = useAuth();

  const [message, setMessage] = useState({
    isShow: false,
    msg: "",
  });

  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e, type) => {
    setData({
      ...data,
      [type]: e.target.value,
    });
  };

  const apiUrl = process.env.REACT_APP_API_URL;

  const handleLogin = async () => {
    try {
      const response = await axios.post(`${apiUrl}User/login`, data);

      const responseData = response?.data;
      if (response.status === 200) {
        const token = responseData.token;
        login(token); // set token to AuthContext
        navigate("/");
      } else {
        setMessage({ isShow: true });
        setApiErrorMsg(responseData.message);
      }
    } catch (error) {
      setMessage({ isShow: true });
      setApiErrorMsg(error.response?.data?.message);
    }
  };

  return (
    <div>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingTop: {
            xs: "3rem",
            sm: "5rem",
          },
          width: {
            xs: "80%",
            sm: "66%",
            md: "50%",
          },
          marginX: "auto",
          position: "relative",
        }}>
        {message.isShow && (
          <Message
            setMessage={setMessage}
            msg={message.msg}
            apiErrorMsg={apiErrorMsg}
          />
        )}
        <Box
          sx={{
            paddingBottom: "1.25rem",
          }}>
          <Box
            sx={{
              paddingBottom: "1.25rem",
            }}>
            <Typography
              sx={{
                fontSize: "2.25rem",
                textAlign: {
                  xs: "center",
                  sm: "left",
                },
              }}>
              Selamat Datang Musikers!
            </Typography>
            <Typography
              sx={{
                fontSize: "1.125rem",
                color: "gray",
                paddingTop: {
                  xs: 0,
                  sm: "0.75rem",
                },
                textAlign: {
                  xs: "center",
                  sm: "left",
                },
              }}>
              Login dulu yuk
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
            }}>
            <Form
              id='email'
              label='Email'
              name='Masukkan Email'
              handleChange={handleChange}
              maxLength={32}
            />
            <Form
              id='password'
              label='Password'
              name='Masukkan Password'
              handleChange={handleChange}
              maxLength={16}
            />

            <Box>
              <div className='pb-5 text-sm text-gray-400 text-right sm:text-right'>
                <NavLink to='/forgotpass'>Lupa kata sandi</NavLink>
              </div>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: {
                  xs: "center",
                  sm: "start",
                },

                flexDirection: {
                  xs: "column",
                  sm: "coloumn",
                },
              }}>
              <Button
                variant='contained'
                onClick={handleLogin}
                className='!w-36 sm:!mr-10 !bg-cornflowerBlue !rounded-md !my-4 !mr-0'
                sx={{
                  width: {
                    xs: "100%",
                    sm: "auto",
                  },
                }}
                disabled={isLoggedIn}>
                Masuk
              </Button>

              <Box
                sx={{
                  fontSize: "1.125rem",
                }}>
                Belum punya akun?{" "}
                <NavLink to='/register'>
                  <Typography
                    sx={{
                      color: "#1976d2",
                      "&:hover": {
                        cursor: "pointer",
                        textDecoration: "underline",
                      },
                      fontSize: "1.125rem",
                      display: "inline",
                    }}>
                    Daftar disini
                  </Typography>
                </NavLink>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default LoginPage;
