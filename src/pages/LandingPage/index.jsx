import {
  FavClassCard,
  OfferingCard,
  TypeClassCard,
  Footer,
} from "../../components";
import { Grid, Box, Typography, CardMedia } from "@mui/material";
import BgLandingPage from "../../assets/images/bg-landing-page.jpeg";
import PlayGuitar from "../../assets/images/play-guitar.svg";
import BgPlayGuitar from "../../assets/images/bg-play-guitar.svg";
import { offeringData } from "../../data";
import { useEffect, useState } from "react";
import { useAuth, useAxios } from "../../hooks";

const LandingPage = () => {
  const [course, setCourse] = useState([]);
  const [category, setCategory] = useState([]);

  const {
    response: categoryData,
    loading: loadingCategory,
    error: errorCategory,
    fetchData: fetchCategoryData,
  } = useAxios({
    method: "get",
    url: "CategoryCourse",
  });

  const {
    response: courseData,
    loading: loadingCourse,
    error: errorCourse,
    fetchData: fetchCourseData,
  } = useAxios({
    method: "get",
    url: "Course/WithSchedule",
  });

  useEffect(() => {
    fetchCourseData();
    fetchCategoryData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const courseResponse = await fetchCourseData();
        const categoryResponse = await fetchCategoryData();

        if (courseResponse.ok && categoryResponse.ok) {
          const courseData = await courseResponse.json();
          const categoryData = await categoryResponse.json();

          if (courseData !== null) {
            setCourse(courseData);
          }
          if (categoryData !== null) {
            setCategory(categoryData);
          }
        } else {
          console.error(
            "Error fetching data:",
            courseResponse.status,
            categoryResponse.status
          );
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    const pollingInterval = setInterval(fetchData, 5000);
    return () => {
      clearInterval(pollingInterval);
    };
  }, []);

  const activecourseData =
    courseData &&
    courseData
      .filter((method) => method.isActive)
      ?.filter((course) => course?.listSchedule?.length != 0);
  const activecategoryData =
    categoryData && categoryData.filter((method) => method.isActive);

  return (
    <Box>
      <CardMedia
        image={BgLandingPage}
        sx={{
          width: "100%",
          height: "auto",
          padding: {
            xs: 2,
            md: 5,
          },
        }}>
        <Typography
          sx={{
            fontSize: {
              xs: "1.3rem",
              md: "1.8rem",
              xl: "2.2rem",
            },
            fontWeight: "bold",
            textAlign: "center",
            color: "white",
          }}>
          Hi Musiker! Gabung yuk di Apel Music
        </Typography>
        <Typography
          sx={{
            fontSize: {
              xs: "1rem",
              md: "1.4rem",
              xl: "1.8rem",
            },
            color: "white",
            textAlign: "center",
            paddingTop: 3,
          }}>
          Banyak kelas keren yang bisa menunjang bakat bermusik kamu
        </Typography>
        <Box
          sx={{
            display: "flex",
            paddingY: {
              xs: 0,
              md: 5,
            },
            justifyContent: "center",
            alignItems: "center",
            flexDirection: {
              xs: "column",
              md: "row",
            },
          }}>
          {offeringData.map((e, i) => {
            return <OfferingCard total={e.total} desc={e.desc} key={i} />;
          })}
        </Box>
      </CardMedia>

      <Box
        display='flex'
        justifyContent='center'
        alignItems='center'
        flexDirection={"column"}
        flexGrow={1}>
        <Typography
          variant='h5'
          marginBottom={3}
          marginTop={8}
          color={"hsl(239, 61%, 65%)"}
          fontWeight='bold'>
          Explore Kelas Favorit
        </Typography>
        <Grid
          container
          width={"80%"}
          display='flex'
          justifyContent='center'
          alignItems='center'
          sx={{
            width: {
              md: "100vw",
              lg: "80vw",
              xl: "70vw",
            },
          }}>
          {activecourseData?.map((courseItem, index) => (
            <FavClassCard
              key={index}
              items={{
                id: courseItem.id,
                source: courseItem.image,
                type: courseItem.category,
                name: courseItem.title,
                price: courseItem.price,
              }}
            />
          ))}
        </Grid>
      </Box>

      <Box
        display='flex'
        justifyContent='center'
        alignItems='center'
        flexDirection={"column"}
        color={"hsl(239, 82%, 81%)"}
        sx={{ backgroundColor: "hsl(239, 82%, 95%)" }}
        flexGrow={1}>
        <Typography
          variant='h5'
          marginBottom={3}
          marginTop={8}
          color={"hsl(239, 61%, 65%)"}
          fontWeight='bold'>
          Pilih kelas impian kamu
        </Typography>
        <Grid
          container
          display='flex'
          justifyContent='center'
          marginBottom={10}
          sx={{
            width: {
              xs: "90vw",
              md: "70vw",
            },
          }}
          alignItems='center'>
          {activecategoryData?.map((categoryItem, index) => (
            <TypeClassCard
              key={index}
              items={{
                id: categoryItem.id,
                source: `data:image/jpeg;base64,${categoryItem.image}`,
                name: categoryItem.name,
              }}
            />
          ))}
        </Grid>
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: {
            xs: "column",
            md: "row",
          },
          justifyContent: {
            xs: "space-around",
            xl: "center",
          },
          alignItems: {
            xs: "center",
            md: "normal",
          },
          paddingRight: {
            xs: 0,
            sm: "3.5rem",
            lg: "5rem",
          },
          paddingTop: {
            xs: "2rem",
            md: "3rem",
            xl: "5rem",
          },
          paddingBottom: {
            md: "3rem",
          },
        }}>
        <Box
          sx={{
            position: "relative",
            width: {
              xs: "18rem",
              sm: "20rem",
              md: "20rem",
              lg: "30rem",
            },
          }}>
          <Box
            sx={{
              height: {
                xs: "22rem",
                md: "25rem",
                lg: "30rem",
              },
            }}
            component='img'
            src={BgPlayGuitar}
          />
          <Box
            sx={{
              height: {
                xs: "14rem",
                sm: "15rem",
                md: "18rem",
                lg: "22rem",
              },
              position: "absolute",
              right: {
                xs: "-1rem",
                sm: "-0.5rem",
                md: "-1.5rem",
                lg: "1.5rem",
                xl: "5rem",
              },
              bottom: {
                xs: "4rem",
                md: "3rem",
                lg: "3.5rem",
              },
            }}
            component='img'
            src={PlayGuitar}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            paddingTop: {
              xs: "2rem",
              md: "0rem",
            },
            paddingBottom: {
              xs: "2rem",
              sm: "4rem",
            },
            marginLeft: {
              xl: "5rem",
            },
            width: {
              xs: "80%",
              md: "35rem",
              lg: "40rem",
              xl: "55rem",
            },
          }}>
          <Typography
            sx={{
              fontSize: "1.55rem",
              fontWeight: "bold",
              marginBottom: "1.35rem",
              color: "hsl(239, 61%, 65%)",
            }}>
            Benefit ikut Apel Course
          </Typography>
          <Typography
            sx={{
              textAlign: "justify",
              fontSize: "1.15rem",
            }}>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
            eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
            qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
            sed quia non numquam eius modi tempora incidunt ut labore et dolore
            magnam aliquam quaerat voluptatem.
          </Typography>
        </Box>
      </Box>

      <Footer />
    </Box>
  );
};

export default LandingPage;
