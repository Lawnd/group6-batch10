import { Box, Typography, Backdrop, Button } from "@mui/material";
import { useState, useEffect } from "react";
import { MethodPaymentCard } from "../../components";
import { methodPaymentData } from "../../data";
import { useNavigate } from "react-router";
import { useAxios } from "../../hooks";
import axios from "axios";
import useAuth from "../../hooks/useAuth";
import { Notification } from "../../components";
import { ErrorNotification, SuccessNotification } from "../../components/Notification";

const PaymentMethod = (props) => {
  const navigate = useNavigate();
  const { payload } = useAuth();
  const [open, setOpen] = useState(true);
  const [data, setData] = useState(methodPaymentData);
  const [selectedPaymentId, setSelectedPaymentId] = useState(null);
  const [lastInvoiceNumber, setLastInvoiceNumber] = useState(null);
  const [IdOrder, setIdOrder] = useState(null);
  const { TotalPrice, TotalCourse, ScheduleId, cartData, removeCheckout } = props;
  const apiUrl = process.env.REACT_APP_API_URL;


  const [notification, setNotification] = useState({
    isShow: false,
    msg: "",
    type: "", 
  });

  const handleSelectedPayment = (id) => {
    setData((prevData) => {
      return prevData.map((item) =>
        item.id === id
          ? { ...item, isSelected: true }
          : { ...item, isSelected: false }
      );
    });

    setSelectedPaymentId(id);
  };

  const options = {
    headers: {
      Authorization: `Bearer ${payload}`,
      "Content-Type": "application/json",
    },
  };

  const getLastInvoiceNumber = async () => {
    try {
      const response = await axios.get(
        `${apiUrl}OrderDetail/LastInvoiceNumber`,
        options
      );

      if (response.status === 200) {
        const lastInvoiceNumber = response.data;
        setLastInvoiceNumber(lastInvoiceNumber);
      } else {
        console.error("Error Get LastInvoiceNumber.");
      }
    } catch (error) {
      console.error(
        "Error Get LastInvoiceNumber.:",
        error.message
      );
    }
  };

  const {
    response: paymentMethodData,
    loading: loadingPaymentMethod,
    error: errorPaymentMethod,
    fetchData: fetchPaymentMethodData,
  } = useAxios({
    url: "Payment",
    method: "GET",
  });

  useEffect(() => {
    fetchPaymentMethodData();
    getLastInvoiceNumber();
  }, []);

  const addOrderDetails = async (orderId, scheduleIds, lastInvoiceNumber) => {
    const options = {
      headers: {
        Authorization: `Bearer ${payload}`,
        "Content-Type": "application/json",
      },
    };

    try {
      const statusResponses = await Promise.all(
        scheduleIds.map(async (schId) => {
          const detailsData = {
            orderId: orderId,
            scheduleId: schId,
            lastInvoiceNumber: lastInvoiceNumber,
          };

          const response = await axios.post(
            `${apiUrl}OrderDetail/AddOrderDetail`,
            detailsData,
            options
          );

          return response.status;
        })
      );

      return statusResponses;
    } catch (error) {
      ErrorNotification(setNotification, "Terjadi kesalahan saat menambahkan order details!");
      throw error;
    }
  };

  const handleBuy = async () => {

    
    if (selectedPaymentId === null) {
      ErrorNotification(setNotification, "Silahkan pilih metode pembayaran!");
      return;
    }

    const options = {
      headers: {
        Authorization: `Bearer ${payload}`,
        "Content-Type": "application/json",
      },
    };

    const requestData = {
      totalCourse: TotalCourse,
      totalCost: TotalPrice,
      paymentId: selectedPaymentId,
    };

    try {
      const response = await axios.post(
        `${apiUrl}Order/CreateOrder`,
        requestData,
        options
      );

      if (response.status === 201) {
        const orderId = response.data.id;
        setIdOrder(orderId);

        const lastInv = lastInvoiceNumber;
        const scheduleIds = ScheduleId;

        const detailsResponses = await addOrderDetails(
          orderId,
          scheduleIds,
          lastInv
        );

        // Pemeriksaan apakah semua respons adalah 201
        if (detailsResponses.every((status) => status === 201)) {
          
          // SuccessNotification(setNotification, "Selamat Order berhasil dibuat!");
          navigate("/successpurchase");
          

          cartData.forEach((item) => {
            if (item.isSelected) {
              removeCheckout(item.id);
            }
          });
        } else {
          // console.error("Gagal membuat pesanan.");
        }
      } else {
        console.error("Error create Order");
        ErrorNotification(setNotification, "Terjadi kesalahan saat membuat pesanan!");
      }
    } catch (error) {
      ErrorNotification(setNotification, "Terjadi kesalahan saat membuat pesanan!");
    }
  };

  const activePaymentMethods =
    paymentMethodData && paymentMethodData.filter((method) => method.isActive);

  return (
    <Backdrop
      sx={{
        color: "#fff",
        zIndex: 50,
      }}
      open={open}>
        <Notification
            isShow={notification.isShow}
            msg={notification.msg}
            type={notification.type}
          />
      <Box
        sx={{
          width: "23.375rem",
          height: "28rem",
          borderRadius: "1rem",
          backgroundColor: "white",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          alignItems: "center",
          paddingY: "1rem",
        }}>
        <Typography
          sx={{
            color: "black",
            fontSize: "1.25rem",
          }}>
          Pilih Metode Pembayaran
        </Typography>
        <Box>
        {activePaymentMethods?.map((e, i) => {
            return (
              <MethodPaymentCard
                name={e.method}
                ico={e.image}
                key={i}
                id={e.id}
                handleSelectedPayment={handleSelectedPayment}
                isSelected={selectedPaymentId === e.id} 
              />
            );
          })}
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            width: "20.375rem",
          }}>
          <Button
            variant='outlined'
            sx={{ width: "9.5rem", height: "2.6rem" }}
            onClick={props?.handlePayment}>
            Batal
          </Button>

          <Button
            onClick={() => {
              handleBuy(TotalCourse, TotalPrice, selectedPaymentId);
            }}
            variant='contained'
            sx={{
              width: "9.5rem",
              height: "2.6rem",
              backgroundColor: "hsl(239, 61%, 65%)",
            }}>
            Bayar
          </Button>
        </Box>
      </Box>
    </Backdrop>
  );
};

export default PaymentMethod;
