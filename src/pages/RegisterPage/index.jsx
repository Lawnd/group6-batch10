import React, { useState } from "react";
import { Form, Message } from "../../components";
import { Box, Button, Typography } from "@mui/material";
import { NavLink, useNavigate } from "react-router-dom";
import axios from "axios";
import {
  isMatch,
  validateEmail,
  validateName,
  validatePassword,
} from "../../utils/validate";

const RegisterPage = () => {
  const navigate = useNavigate();
  const [apiErrorMsg, setApiErrorMsg] = useState("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const [message, setMessage] = useState({
    isShow: false,
    msg: "",
  });
  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    password2: "",
  });

  const handleChange = (e, type) => {
    setData({
      ...data,
      [type]: e.target.value,
    });
  };

  const apiUrl = process.env.REACT_APP_API_URL;

  const handleRegister = async () => {
    try {
      setIsButtonDisabled(true); //state register true
      if (!validateEmail(data.email)) {
        setMessage({
          isShow: true,
          msg: "email",
        });
        return;
      }
      if (!validatePassword(data.password)) {
        setMessage({
          isShow: true,
          msg: "password",
        });
        return;
      }
      if (!isMatch(data.password, data.password2)) {
        setMessage({
          isShow: true,
          msg: "password",
        });
        return;
      }
      if (!validateName(data.name)) {
        setMessage({
          isShow: true,
          msg: "name",
        });
        return;
      }

      const response = await axios.post(
        `${apiUrl}User/Register`,
        {
          Fullname: data.name,
          Email: data.email,
          Password: data.password,
          Role: false, // Default to User
        }
      );
      

      const responseData = response?.data;
      // console.log("Response from API:", response.status);

      if (response.status === 200) {
        navigate("/emailconfirm");
      } else {
        setMessage({ isShow: true });
        setApiErrorMsg(responseData.message);
      }
    } catch (error) {
      setMessage({ isShow: true });
      setApiErrorMsg(error.response?.data?.message);
    } finally {
      setIsButtonDisabled(false);
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        paddingTop: {
          xs: "3rem",
          sm: "5rem",
        },
        width: {
          xs: "80%",
          sm: "66%",
          md: "50%",
        },
        marginX: "auto",
        position: "relative",
      }}>
      {message.isShow && (
        <Message
          setMessage={setMessage}
          msg={message.msg}
          apiErrorMsg={apiErrorMsg}
        />
      )}
      <Box
        sx={{
          paddingBottom: "1.25rem",
        }}>
        <Typography
          sx={{
            fontSize: "2.25rem",
            textAlign: {
              xs: "center",
              sm: "left",
            },
          }}>
          Selamat Datang Musikers!
        </Typography>
        <Typography
          sx={{
            fontSize: "1.125rem",
            color: "gray",
            paddingTop: {
              xs: 0,
              sm: "0.75rem",
            },
            textAlign: {
              xs: "center",
              sm: "left",
            },
          }}>
          Yuk daftar terlebih dahulu akun kamu
        </Typography>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
        }}>
        <Form
          id='name'
          label='Name Lengkap'
          name='Masukkan Nama Lengkap'
          handleChange={handleChange}
          maxLength={32}
        />
        <Form
          id='email'
          label='Email'
          name='Masukkan Email'
          handleChange={handleChange}
          maxLength={32}
        />
        <Form
          id='password'
          label='Password'
          name='Masukkan Password'
          handleChange={handleChange}
          maxLength={16}
        />
        <Form
          id='password2'
          label='Password'
          name='Konfirmasi Password'
          handleChange={handleChange}
          maxLength={16}
        />
        <Box>
          <ul>
            <li className='text-gray-700 list-disc list-inside'>
              The password length should be more than 5 characters.
            </li>
            <li className='text-gray-700 list-disc list-inside'>
              The password length should be less than 17 characters.
            </li>
            <li className='text-gray-700 list-disc list-inside'>
              The password should contain at least 1 number and at least 1
              special character.
            </li>
          </ul>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: {
              xs: "center",
              sm: "start",
            },
            alignItems: "center",
            flexDirection: {
              xs: "column",
              sm: "row",
            },
          }}>
          <Button
            variant='contained'
            onClick={handleRegister}
            className={`!w-36 sm:!mr-10 !bg-cornflowerBlue !rounded-md !my-4 !mr-0`}
            disabled={isButtonDisabled}>
            Daftar
          </Button>
          <Box
            sx={{
              fontSize: "1.125rem",
            }}>
            Sudah punya akun?{" "}
            <NavLink to='/login'>
              <Typography
                sx={{
                  color: "#1976d2",
                  "&:hover": { cursor: "pointer", textDecoration: "underline" },
                  fontSize: "1.125rem",
                  display: "inline",
                }}>
                Login disini
              </Typography>
            </NavLink>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default RegisterPage;
