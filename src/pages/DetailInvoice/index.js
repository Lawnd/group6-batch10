import React from "react";
import Footer from "../../components/Footer";
import { NavLink } from "react-router-dom";
import { InvoiceDetailTableRow } from "../../components";
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Grid,
    Typography,
    Breadcrumbs,
    Link,
  } from "@mui/material";
import useAuth from "../../hooks/useAuth";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import { dateFormat, priceIDFormat } from '../../utils/formatter';

const DetailInvoice = () => {
    const navigate = useNavigate();
    const apiUrl = process.env.REACT_APP_API_URL;
    const { payload } = useAuth(); 
    const [invoiceData, setInvoiceData] = useState([]);
    const [detailInvoiceData, setDetailInvoiceData] = useState([]);
    const { id } = useParams();

    

    const options = {
        headers: {
          Authorization: `Bearer ${payload}`,
          'Content-Type': 'application/json',
        },
      };


    const getInvoiceData = async () => {
        try {
          if (!payload) {
            navigate("/login");
            return;
          }
    
          const response = await axios.get(`${apiUrl}Invoice/GetInvoiceById?id_order=${id}`, options);
    
          if (response.status === 200) {
            const invoiceData = response.data; 
            setInvoiceData(invoiceData);
          } else {     
          }
        } catch (error) {
        }
      };
      

      const getDetailInvoiceData = async () => {
        try {
          if (!payload) {
            navigate("/login");
            return;
          }
    
          const response = await axios.get(`${apiUrl}Invoice/GetInvoiceDetail?id_order=${id}`, options);
    
          if (response.status === 200) {
            const detailInvoiceData = response.data; 
            setDetailInvoiceData(detailInvoiceData);
          } else {
          }
        } catch (error) {
        }
      };

      useEffect(() => {
        getInvoiceData();
        getDetailInvoiceData();
      }, [payload]);

      
    return(
        <div>
            <Grid container 
            sx={{
                display: 'flex',
                gridGap: '16px', 
                padding: 1,
                justifyContent: 'center', 
                alignItems: 'center', 
                paddingX: 5,
                marginBottom: 20,
              }}
            >
            
            <Box 
            sx={{     
            width: {xs:"300px", sm:"800px", md:"1000px", lg:"1500px"},
            marginTop: 2
            }}>
            
            <Breadcrumbs separator="›" aria-label="breadcrumb">
            <Typography fontWeight='bold'>    
                <Link underline="hover" key="1" color="inherit" 
                component={NavLink}
                to="/"
                >
                Beranda
                </Link>   
            </Typography>

            <Typography fontWeight='bold'>
                <Link underline="hover" key="1" color="inherit" 
                component={NavLink}
                to="/invoice"
                >
                Invoice
                </Link>
            </Typography>    
                
            <Typography key="3" color={"hsl(239, 61%, 65%)"} fontWeight='bold'>
            Rincian Invoice
            </Typography>
            </Breadcrumbs>
        

            <Typography
            variant='h6'
            component='div'
            fontWeight='bold'
            marginTop={5}
            >
            Rincian Invoice
            </Typography>

            </Box>

            <Box 
            sx={{ width: { xs:"300px", sm:"800px", md:"1000px", lg:"1500px"}}} >
            
            <Box 
             sx={{
                display: 'flex',
                flexDirection: {
                  xs: 'column', 
                  sm: 'row',    
                },
                alignItems: {
                    xs: 'flex-start',
                    sm: 'flex-end'
                },
                justifyContent: {
                    sm: 'space-between',
                    xs: 'flex-start'
                }
                
              }}
            > 
                <Box>
                    <Typography
                    component='div'
                    fontWeight='medium'
                    >
                    No. Invoice : {invoiceData.noInvoice}
                    </Typography>
                    <Typography
                    component='div'
                    fontWeight='medium'
                    marginTop={1}
                    >
                    Tanggal Beli  : {dateFormat(invoiceData.created)}
                    </Typography>
                </Box>
                <Box>
                    <Typography
                    component='div'
                    fontWeight='bold'
                    
                    >
                    Total Harga {invoiceData.totalCost ? priceIDFormat(invoiceData.totalCost) : "Loading..."}
                    </Typography>
                </Box>
            </Box>

            </Box>

            <Box 
            sx={{ width: { xs:"300px", sm:"800px", md:"1000px", lg:"1500px"}}} > 
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                <TableHead style={{ backgroundColor: "#F2C94C", color: "white" }}>
                    <TableRow>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>No</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Nama Course</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Kategori</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Jadwal</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Harga</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {detailInvoiceData.map((item, index) => (
                    <InvoiceDetailTableRow
                        key={index}
                        index={index}
                        item={{
                        no: index + 1,
                        namecourse: item.title,
                        category: item.category,
                        schedule: item.date,
                        price: item.price,
                        }}
                    />
                    ))}

                </TableBody>
                </Table>
            </TableContainer>
            </Box>

            </Grid>

            <Footer />
        </div>
   );
}

export default DetailInvoice;