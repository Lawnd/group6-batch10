import { Box, Button, Typography } from "@mui/material";
import { Form, Message } from "../../components";
import { useState, useEffect } from "react";
import { isMatch, validatePassword } from "../../utils/validate";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useLocation } from "react-router-dom";



const ResetPassword = () => {
  const navigate = useNavigate();
  const [isButtonDisabled, setIsButtonDisabled] = useState(false); 
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const email = searchParams.get("email");

  const [message, setMessage] = useState({
    isShow: false,
    msg: "",
  });

  const [data, setData] = useState({
    email: "", 
    password1: "",
    password2: "",
  });

  const handleChange = (e, type) => {
    setData({
      ...data,
      [type]: e.target.value,
    });
  };



  const apiUrl = process.env.REACT_APP_API_URL;

  const handleReset = async () => {
    setIsButtonDisabled(true);

    if (
      !validatePassword(data.password1) ||
      !isMatch(data.password1, data.password2)
    ) {
      setMessage({
        isShow: true,
        msg: "password",
      });
      setIsButtonDisabled(false);
      return;
    }

    const resetData = {
      password: data.password1,
      confirmPassword: data.password2,
      email: email, 
    };
   
    try {
      const resetResponse = await axios.post(
        `${apiUrl}User/ResetPassword`,
        resetData
      );

      const resetResult = resetResponse?.data;
      if (resetResponse.status === 200) {
        navigate("/emailpass");
      } else {
        setMessage({ isShow: true });
      }
    } catch (error) {
      setMessage({ isShow: true });
    } finally {
      setIsButtonDisabled(false);
    }
  };
  




  return (
    <Box
      display='flex'
      flexDirection='column'
      marginX='auto'
      sx={{
        paddingTop: {
          xs: "3rem",
          sm: "5rem",
        },
        width: {
          xs: "80%",
          sm: "67%",
          lg: "50%",
        },
        position: "relative",
      }}>
      {message.isShow && <Message setMessage={setMessage} msg={message.msg} />
      }
      <Typography fontSize='2.25rem'>Buat Password</Typography>
      <Box paddingTop={3} />
      <Form
        id='password1'
        label='Password'
        name='Masukkan password baru'
        handleChange={handleChange}
        maxLength={16}
      />
      <Form
        id='password2'
        label='Password'
        name='Konfirmasi password baru'
        handleChange={handleChange}
        maxLength={16}
      />
      <Box>
        <ul>
          <li className='text-gray-700 list-disc list-inside'>
            The password length should be more than 5 characters.
          </li>
          <li className='text-gray-700 list-disc list-inside'>
            The password length should be less than 17 characters.
          </li>
          <li className='text-gray-700 list-disc list-inside'>
            The password should contain at least 1 number and at least 1 special
            character.
          </li>
        </ul>
      </Box>
      <Box paddingTop={3}>
        <Button
          onClick={() => navigate("/forgotpass")}
          sx={{
            borderColor: "hsl(239, 61%, 65%)",
            border: 1,
            paddingX: 3,
            paddingY: 1,
            width: "20%",
            maxWidth: 150,
            minWidth: 115,
            "&:hover": {
              backgroundColor: "hsl(239, 61%, 65%)",
              color: "white",
            },
          }}>
          Batal
        </Button>

        <Button
          onClick={handleReset}
          sx={{
            backgroundColor: "hsl(239, 61%, 65%)",
            color: "white",
            border: 1,
            paddingX: 3,
            paddingY: 1,
            marginLeft: 3,
            width: "20%",
            maxWidth: 150,
            minWidth: 115,

            "&:hover": {
              backgroundColor: "hsl(239, 61%, 75%)",
              border: 1,
              borderColor: "hsl(239, 61%, 65%)",
              color: "hsl(239, 61%, 45%)",
            },
          }}
          disabled={isButtonDisabled}
          >
          Kirim
        </Button>
      </Box>
    </Box>
  );
};

export default ResetPassword;
