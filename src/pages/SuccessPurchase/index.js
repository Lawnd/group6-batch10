import { NavLink } from "react-router-dom";
import React from "react";
import { Container, Button, Box, Typography } from "@mui/material";
import success from "../../assets/images/email-succes.svg";
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from "react-router-dom";

const SuccessPurchase = () => {
    const navigate = useNavigate();
    
    return (
     <>
        <Container component="main" 
        sx={{ mb: 5, mt: 5,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"}}  >

            <Box
                sx={{
                    height: {
                    xs: "15rem",
                    md: "18rem",
                    lg: "20rem",
                },
                }}
                component='img'
                src={success}
            />
            
            <Typography
            sx={{
              fontSize: "1.55rem",
              fontWeight: "bold",
              marginBottom: "1.35rem",
              color: "hsl(239, 61%, 65%)",
            }}>
            Pembelian Berhasil
            </Typography>
            <Typography
                sx={{
                textAlign: "center",
                fontSize: "1.15rem",
                }}>
                Yey! Kamu telah berhasil membeli kursus di Apel Music
            </Typography>
            
            <Box 
            display="flex"
            justifyContent="center"
            sx={{ gap: '16px' }}
            > 
            <NavLink to='/'>
            <Button
            sx={{
                borderColor: "hsl(239, 61%, 65%)",
                border: 1,
                paddingX: 2,
                paddingY: 0.5,
                width: 170,
                height: 50,
                marginTop: 3,
                display: 'flex',
                justifyContent: 'center',
                "&:hover": {
                backgroundColor: "hsl(239, 61%, 65%)",
                color: "white",          
                },
            }}>
            <HomeIcon fontSize="medium"/> Ke Beranda
            </Button>
            </NavLink>

            <Button
            onClick={() => navigate("/invoice")}
            sx={{
                backgroundColor: "hsl(239, 61%, 65%)",
                color: "white",
                border: 1,
                paddingX: 2,
                paddingY: 0.5,
                width: 170,
                height: 50,
                marginTop: 3,
                marginLeft: {xs: 0, md:1},
                display: 'flex',
                justifyContent: 'center',
                "&:hover": {
                  backgroundColor: "hsl(239, 61%, 75%)",
                  border: 1,
                  borderColor: "hsl(239, 61%, 65%)",
                  color: "hsl(239, 61%, 45%)",
                    },
                }}>
                <ArrowForwardIcon fontSize="medium"/>Buka Invoice
            </Button>
            </Box>
        </Container>
     </>
    ) 
}

export default SuccessPurchase;