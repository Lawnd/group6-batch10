import { Box, Button, Typography } from "@mui/material";
import { Form, Message } from "../../components";
import { useState } from "react";
import { validateEmail } from "../../utils/validate";
import { useNavigate } from "react-router-dom";
import axios from "axios";



const ForgotPassword = () => {
  const navigate = useNavigate();
  const [apiErrorMsg, setApiErrorMsg] = useState(""); // message error from API
  const [isButtonDisabled, setIsButtonDisabled] = useState(false); 

  const [message, setMessage] = useState({
    isShow: false,
    msg: "",
  });

  const [data, setData] = useState({
    email: "",
  });

  const handleChange = (e, type) => {
    setData({
      ...data,
      [type]: e.target.value,
    });
  };

  const apiUrl = process.env.REACT_APP_API_URL;
  const emailQueryParam = `email=${data.email}`;

  const handleForgetPassword = async () => {
    setIsButtonDisabled(true); 

    if (!validateEmail(data.email)) {
      setMessage({
        isShow: true,
        msg: "email",
      });
      return;
    }
    
    try {
      const resetResponse = await axios.post(
      `${apiUrl}User/ForgetPassword?${emailQueryParam}`,
    );

      const resetData = resetResponse?.data;
      if (resetResponse.status === 200) {
        setMessage({ isShow: true, msg: "resetSuccess" });
        navigate("/emailnotification");
      } else {
        setMessage({ isShow: true });
        setApiErrorMsg("Gagal Melakukan Reset Password");
       
      }
    } catch (error) {
      setMessage({ isShow: true });
      setApiErrorMsg("Gagal Melakukan Reset Password");
      // setApiErrorMsg(error.response?.data?.message);
      
    } finally {
      setIsButtonDisabled(false);
    }
  };
  

  return (
    <Box
      display='flex'
      flexDirection='column'
      marginX='auto'
      sx={{
        paddingTop: {
          xs: "3rem",
          sm: "5rem",
        },
        width: {
          xs: "80%",
          sm: "67%",
          lg: "50%",
        },
        position: "relative",
      }}>
      {message.isShow && 
        <Message
        setMessage={setMessage}
        msg={message.msg}
        apiErrorMsg={apiErrorMsg}
        />
      }
      <Typography fontSize='2.25rem'>Reset Password</Typography>
      <Typography fontSize='1.125rem' color='gray'>
        Silahkan masukan terlebih dahulu email anda
      </Typography>
      <Box paddingTop={3} />
      <Form
        id='email'
        label='Email'
        name='Masukkan Email'
        handleChange={handleChange}
        maxLength={32}
      />
      <Box paddingTop={4}>
        <Button
          onClick={() => navigate(-1)}
          sx={{
            borderColor: "hsl(239, 61%, 65%)",
            border: 1,
            paddingX: 3,
            paddingY: 1,
            width: "20%",
            maxWidth: 150,
            minWidth: 115,
            "&:hover": {
              backgroundColor: "hsl(239, 61%, 65%)",
              color: "white",
            },
          }}>
            
          Batal
        </Button>

        <Button
          onClick={handleForgetPassword}
          sx={{
            backgroundColor: "hsl(239, 61%, 65%)",
            color: "white",
            border: 1,
            paddingX: 3,
            paddingY: 1,
            marginLeft: 3,
            width: "20%",
            maxWidth: 150,
            minWidth: 115,

            "&:hover": {
              backgroundColor: "hsl(239, 61%, 75%)",
              border: 1,
              borderColor: "hsl(239, 61%, 65%)",
              color: "hsl(239, 61%, 45%)",
            },
          }}
          disabled={isButtonDisabled}
          >
          Konfirmasi
        </Button>
      </Box>
    </Box>
  );
};

export default ForgotPassword;
