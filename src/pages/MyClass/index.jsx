import React from "react";
import Footer from "../../components/Footer";
import {  Box } from "@mui/material";
import { checkoutDataDummy } from "../../data";
import { MyClassCard } from "../../components";
import { useNavigate } from "react-router-dom";
import { dateFormat, priceIDFormat } from '../../utils/formatter';
import { useEffect, useState } from "react";
import useAuth from "../../hooks/useAuth";
import axios from "axios";


 

const MyClass = () =>{
  const navigate = useNavigate();
  const apiUrl = process.env.REACT_APP_API_URL;
  const { payload } = useAuth(); 
  const [classData, setClassData] = useState([]);

  const options = {
    headers: {
      Authorization: `Bearer ${payload}`,
      'Content-Type': 'application/json',
    },
  };

  const getInvoiceData = async () => {
    try {
      if (!payload) {
        navigate("/login");
        return;
      }

      const response = await axios.get(`${apiUrl}Invoice/GetMyClass`, options);

      if (response.status === 200) {
        const classData = response.data; 
        setClassData(classData);
      } else {     
      }
    } catch (error) {
    }
  };

  useEffect(() => {
    getInvoiceData();
  }, [payload]);


    return(
        <div>
            {classData.map((v, i) => {
              return (
                <MyClassCard
                  key={i}
                  item={{
                    img: `data:image/jpeg;base64,${v.image}`,
                    type: v.category,
                    name: v.title,
                    date: v.date,
                  }}
                />
              );
            })}


            <Box
            sx={{
                height: 100,
                width: "100%",
                display: "flex", 
                paddingY: 2,
                alignItems: 'center'
            }}> 
            </Box>

        <Footer />

        </div>
    );
}

export default MyClass