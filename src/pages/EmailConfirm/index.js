import { NavLink } from "react-router-dom";
import React from "react";
import { Container, Button, Box, Typography } from "@mui/material";
import success from "../../assets/images/email-succes.svg";

const EmailConfirm = () => {
    
    
    return (
     <>
        <Container component="main" 
        sx={{ mb: 5, mt: 5,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"}}  >

            <Box
                sx={{
                    height: {
                    xs: "15rem",
                    md: "18rem",
                    lg: "20rem",
                },
                }}
                component='img'
                src={success}
            />
            
            <Typography
            sx={{
              fontSize: "1.55rem",
              fontWeight: "bold",
              marginBottom: "1.35rem",
              color: "hsl(239, 61%, 65%)",
            }}>
            Konfirmasi Email Berhasil
            </Typography>
            <Typography
                sx={{
                textAlign: "center",
                fontSize: "1.15rem",
                }}>
                Silahkan cek Email anda untuk melakukan aktivasi akun
            </Typography>

            
        </Container>
     </>
    ) 
}

export default EmailConfirm;