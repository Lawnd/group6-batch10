import RegisterPage from "./RegisterPage";
import LoginPage from "./LoginPage";
import ForgotPassword from "./ForgotPassword";
import ResetPassword from "./ResetPassword";
import LandingPage from "./LandingPage";
import EmailConfirm from "./EmailConfirm";
import CheckoutPage from "./CheckoutPage";
import PaymentMethodPage from "./PaymentMethod";
import MenuClassPage from "./MenuClassPage";
import DetailClass from "./DetailClass";
import SuccessPurchase from "./SuccessPurchase";
import Invoice from "./Invoice";
import MyClass from "./MyClass";
import DetailInvoice from "./DetailInvoice";
import EmailCheck from "./EmailCheck";
import EmailConfirmPass from "./EmailConfirmPass";

export { 
    RegisterPage, 
    LoginPage, 
    ResetPassword, 
    ForgotPassword, 
    LandingPage,
    CheckoutPage,
    PaymentMethodPage,
    EmailConfirm,
    MenuClassPage,
    MyClass,
    DetailClass,
    SuccessPurchase,
    Invoice,
    DetailInvoice,
    EmailCheck,  
    EmailConfirmPass
    };