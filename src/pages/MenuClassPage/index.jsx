import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Box, Grid, Typography } from "@mui/material";
import bgMenuClass from "../../assets/images/class6.jpeg";
import { ClassDescriptionBox, FavClassCard } from "../../components";
import Footer from "../../components/Footer";
import { useAxios } from "../../hooks";
import axios from "axios"; // Import Axios

const MenuClassPage = () => {
  const [course, setCourse] = useState([]);
  const [category, setCategory] = useState([]);
  const { id } = useParams();

  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    if (id) {
      axios
        .get(`${apiUrl}CategoryCourse/GetById?id=${id}`)
        .then((response) => {
          const categoryData = response.data;
          setCategory(categoryData);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  }, [id]);

  const {
    response: courseData,
    loading: loadingCourse,
    error: errorCourse,
    fetchData: fetchCourseData,
  } = useAxios({
    method: "get",
    url: "Course",
  });

  useEffect(() => {
    fetchCourseData();
  }, []);

  const activecourseData =
    courseData? courseData.filter((items) => items.isActive && items.category === category.name)
    :[] ;

  return (
    <div>
      <Box>
        {category &&
          category.image &&
          category.name &&
          category.description && (
            <ClassDescriptionBox
              src={`data:image/jpeg;base64,${category.image}`}
              title={category.name}
              description={category.description}
            />
          )}
        <Box
          display='flex'
          justifyContent='center'
          alignItems='center'
          flexDirection={"column"}
          flexGrow={1}>
          <Typography
            variant='h5'
            marginBottom={3}
            marginTop={8}
            color={"hsl(239, 61%, 65%)"}
            fontWeight='bold'>
            Kelas yang tersedia
          </Typography>
          <Grid
            container
            width={"80%"}
            display='flex'
            justifyContent='center'
            alignItems='center'
            sx={{
              width: {
                md: "100vw",
                lg: "80vw",
                xl: "70vw",
              },
            }}>
            {activecourseData?.map((courseItem, index) => (
              <FavClassCard
                key={index}
                items={{
                  id: courseItem.id,
                  source: courseItem.image,
                  type: courseItem.category,
                  name: courseItem.title,
                  price: courseItem.price,
                }}
              />
            ))}
          </Grid>
        </Box>
        <Footer />
      </Box>
    </div>
  );
};

export default MenuClassPage;
