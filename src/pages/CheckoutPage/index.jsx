import { useState } from "react";
import { Box, Button, Checkbox, Typography } from "@mui/material";
import { CheckoutCard } from "../../components";
import { checkoutDataDummy } from "../../data";
import { priceIDFormat } from "../../utils/formatter";
import PaymentMethod from "../PaymentMethod";
import useAuth from "../../hooks/useAuth";
import axios from "axios";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Notification } from "../../components";
import { ErrorNotification } from "../../components/Notification";

const CheckoutPage = () => {
  const navigate = useNavigate();
  const [selectAll, setSelectAll] = useState(false);
  const [checkoutData, setCheckoutData] = useState(checkoutDataDummy);
  const [showPayment, setShowPayment] = useState(false);
  const { payload } = useAuth();
  const [cartData, setCartData] = useState([]);
  const [deletedItemIds, setDeletedItemIds] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalCourse, setTotalCourse] = useState(0);
  const [selectedScheduleIds, setSelectedScheduleIds] = useState([]);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const apiUrl = process.env.REACT_APP_API_URL;

  const [notification, setNotification] = useState({
    isShow: false,
    msg: "",
    type: "",
  });

  const handlePayment = () => {
    setShowPayment(!showPayment);
  };

  const handleSelectAll = () => {
    const isAllSelected = cartData.every((item) => item.isSelected);

    const updatedData = cartData.map((item) => ({
      ...item,
      isSelected: !isAllSelected,
    }));

    setCartData(updatedData);
    setSelectAll(!isAllSelected);
  };

  const handleSelect = (id) => {
    const updatedData = cartData.map((item) =>
      item.id === id ? { ...item, isSelected: !item.isSelected } : item
    );

    setCartData(updatedData);
    const isAnyUnchecked = updatedData.some((item) => !item.isSelected);

    if (isAnyUnchecked) {
      setSelectAll(false);
    } else {
      setSelectAll(true);
    }
  };
  const options = {
    headers: {
      Authorization: `Bearer ${payload}`,
      "Content-Type": "application/json",
    },
  };

  const removeCheckout = async (id) => {
    try {
      const response = await axios.delete(
        `${apiUrl}Cart/DeleteCart?userId=${id}`,
        options
      );
  
      if (response.status === 204) {
        const updatedData = checkoutData.filter((value) => value.id !== id);
        setCheckoutData(updatedData);
        updateSelectedItems(id); // Perbarui item yang terpilih
      } else {
        ErrorNotification(setNotification, "Gagal menghapus item dari keranjang!");
      }
    } catch (error) {
      ErrorNotification(
        setNotification, "Gagal menghapus item dari keranjang!");
    }
    setDeletedItemIds([...deletedItemIds, id]);
  };
  

  const updateSelectedItems = (id) => {
    const updatedCartData = cartData.map((item) =>
      item.id === id ? { ...item, isSelected: false } : item
    );
    setCartData(updatedCartData);
  };
  

  useEffect(() => {
    const getCartData = async () => {
      try {
        if (!payload) {
          navigate("/login");
          return;
        }

        const response = await axios.get(
          `${apiUrl}Cart/GetCartByIdUser`,
          options
        );
        if (response.status === 200) {
          const cartData = response.data.map((item) => ({
            ...item,
            isSelected: true,
          }));
          setCartData(cartData);
          const isAllSelected = cartData.every((item) => item.isSelected);
          setSelectAll(isAllSelected);
        } else {
          ErrorNotification(
            setNotification,
            "Gagal menghapus item dari keranjang!"
          );
        }
      } catch (error) {
        ErrorNotification(
          setNotification,
          "Gagal menghapus item dari keranjang!"
        );
      }
    };

    getCartData();
  }, [payload]);

  const getSelectedScheduleIds = () => {
    const selectedSchedules = cartData.filter((item) => item.isSelected);
    const selectedScheduleIds = selectedSchedules.map(
      (item) => item.scheduleId
    );
    return selectedScheduleIds;
  };

  useEffect(() => {
    const totalPrice = cartData
      .filter((item) => item.isSelected)
      .reduce((total, item) => total + item.price, 0);
    setTotalPrice(totalPrice);

    const selectedCoursesCount = cartData.filter(
      (item) => item.isSelected
    ).length;
    setTotalCourse(selectedCoursesCount);

    const ids = getSelectedScheduleIds();
    setSelectedScheduleIds(ids);

    setIsButtonDisabled(cartData.every((item) => !item.isSelected));
  }, [cartData]);

  return (
    <>
      {showPayment ? (
        <PaymentMethod
          handlePayment={handlePayment}
          TotalPrice={totalPrice}
          TotalCourse={totalCourse}
          ScheduleId={selectedScheduleIds}
          cartData={cartData}
          removeCheckout={removeCheckout}
        />
      ) : (
        ""
      )}
      <Box
        sx={{
          width: {
            xs: "87%",
            md: "55rem",
            lg: "70rem",
            xl: "80rem",
          },
          display: "flex",
          justifyContent: "start",
          alignItems: "center",
          flexDirection: "column",
          marginX: "auto",
          marginTop: "1.5rem",
        }}>
        <Notification
          isShow={notification.isShow}
          msg={notification.msg}
          type={notification.type}
        />
        <Box
          sx={{
            height: {
              xs: "2.5rem",
              lg: "3rem",
            },
            width: "100%",
            display: "flex",
            alignItems: "center",
            borderBottom: "0.05rem",
            borderBottomColor: "black",
            borderStyle: "solid",
          }}>
          <Checkbox checked={selectAll} onClick={handleSelectAll} />
          <Typography sx={{ paddingLeft: "1.75rem" }}>Pilih Semua</Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            width: "100%",
            marginBottom: "5rem",
          }}>
          {cartData.map((e) => {
            if (deletedItemIds.includes(e.id)) {
              return null;
            }

            const isSelected = e.isSelected;

            return (
              <CheckoutCard
                key={e.id}
                items={{
                  id: e.id,
                  image: e.image,
                  date: e.date,
                  category: e.category,
                  title: e.title,
                  price: e.price,
                }}
                isSelected={isSelected}
                removeCheckout={removeCheckout}
                handleSelect={() => handleSelect(e.id)}
              />
            );
          })}
        </Box>
        <Box
          sx={{
            height: {
              xs: "5rem",
              sm: "5rem",
              xl: "5rem",
            },
            margin: 0,
            width: "100vw",
            borderStyle: "solid",
            position: "fixed",
            borderTop: 2,
            borderTopColor: "gainsboro",
            bottom: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "white",
          }}>
          <Box
            sx={{
              width: {
                xs: "87%",
                md: "55rem",
                lg: "70rem",
                xl: "80rem",
              },
              height: {
                xs: "0rem",
                xl: "60%",
              },
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}>
            <Box
              sx={{
                display: "flex",
                flexDirection: { xs: "column", sm: "row" },
                alignItems: {
                  xs: "start",
                  sm: "center",
                },
              }}>
              <Typography>Total Biaya </Typography>
              <Typography
                sx={{
                  paddingLeft: {
                    xs: 0,
                    sm: "0.5rem",
                  },
                  fontSize: "1.2rem",
                  color: "#5D5FEF",
                  fontWeight: "bold",
                }}>
                {priceIDFormat(totalPrice)}
              </Typography>
            </Box>
            <Button
              onClick={handlePayment}
              sx={{
                backgroundColor: "#5D5FEF",
                color: "white",
                width: "10rem",
                display: {
                  xs: "none",
                  sm: "block",
                },
                "&:hover": {
                  backgroundColor: "#3A3ACD",
                },
              }}
              disabled={isButtonDisabled}>
              Bayar Sekarang
            </Button>
            <Button
              sx={{
                backgroundColor: "#5D5FEF",
                color: "white",
                width: "7rem",
                display: {
                  xs: "block",
                  sm: "none",
                },
              }}>
              Bayar
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default CheckoutPage;
