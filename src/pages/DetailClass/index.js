/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import {
  Grid,
  Box,
  Typography,
  Button,
  TextField,
  Autocomplete,
} from "@mui/material";
import { FavClassCard, Notification } from "../../components";
import Footer from "../../components/Footer";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useAxios, useAuth } from "../../hooks";
import axios from "axios";
import { priceIDFormat, dateFormat } from "../../utils/formatter";
import { ErrorNotification, SuccessNotification } from "../../components/Notification";


const DetailClass = () => {
  const navigate = useNavigate();
  const [course, setCourse] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [selectedOption, setSelectedOption] = useState(null);
  const [selectedId, setSelectedId] = useState(null);
  const { id } = useParams();
  const { payload } = useAuth();

  const [notification, setNotification] = useState({
    isShow: false,
    msg: "",
    type: "", 
  });
  

  const {
    response: courseDataById,
    loading: loadingCourseById,
    error: errorCourseById,
    fetchData: fetchCourseDataById,
  } = useAxios({
    method: "get",
    url: `Course/GetById?id=${id}`,
  });

  const {
    response: courseSchedule,
    loading: loadingcourseSchedule,
    error: errorcourseSchedule,
    fetchData: fetchcourseSchedule,
  } = useAxios({
    method: "get",
    url: `Course/WithScheduleId?courseId=${id}`,
  });

  const {
    response: courseData,
    loading: loadingCourse,
    error: errorCourse,
    fetchData: fetchCourseData,
  } = useAxios({
    method: "get",
    url: "Course",
  });

  useEffect(() => {
    fetchCourseDataById();
    fetchcourseSchedule();

    setInputValue("");
    setSelectedOption(null);
    setSelectedId(null);
  }, [id]);

  useEffect(() => {
    fetchCourseData();
  }, []);

  const scheduleList = courseSchedule?.listSchedule || [];

  const options = scheduleList.map((schedule) => ({
    id: schedule.id,
    date: dateFormat(schedule.date),
  }));

  const apiUrl = process.env.REACT_APP_API_URL;

  const AddCartAction = async () => {
    try {
      const response = await axios.post(
        `${apiUrl}Cart/AddCart`,
        { scheduleId: selectedOption?.id },
        {
          headers: {
            Authorization: `Bearer ${payload}`,
            "Content-Type": "application/json",
          },
        }
      );

      if (response && response.data.success) {
        return true;
      }
    } catch (error) {
      throw error;
    }

    return false;
  };

  const handleCart = async () => {
    try {
      if (!payload) {
        ErrorNotification(setNotification, "Silahkan login dahulu");

        setTimeout(() => {
          navigate("/login");
        }, 2000);

        return;
      }

      const success = await AddCartAction();
      if (!success) {
        SuccessNotification(setNotification, "Item berhasil dimasukkan ke dalam keranjang!");
      } else {
        ErrorNotification(setNotification, "Item gagal dimasukkan ke dalam keranjang!");
      }
    } catch (error) {
      ErrorNotification(setNotification, "Item gagal dimasukkan, Silahkan pilih jadwal dahulu!");
    }
  };

  const handleBuy = async () => {
    try {
      if (!payload) {
        ErrorNotification(setNotification, "Silahkan login dahulu");
        
        setTimeout(() => {
          navigate("/login");
        }, 2000);

        return;
      }

      const success = await AddCartAction();
      if (!success) {
        SuccessNotification(setNotification, "Item berhasil dimasukkan ke dalam keranjang!");
        setTimeout(() => {
          navigate("/checkoutpage");
        }, 1500);

      } else {
        ErrorNotification(setNotification, "Item gagal dimasukkan ke dalam keranjang!");
      }
    } catch (error) {
      ErrorNotification(setNotification, "Item gagal dimasukkan, Silahkan pilih jadwal dahulu!");
    }
  };

  const activecourseData =
    courseData && courseData.filter((method) => method.isActive);

  return (
    <div>
      <Grid
        container
        sx={{
          display: "flex",
          gridGap: "16px",
          padding: 1,
          justifyContent: "center",
          alignItems: "center",
          paddingX: 5,
        }}>
          <Notification
            isShow={notification.isShow}
            msg={notification.msg}
            type={notification.type}
          />
        <Box
          sx={{
            textAlign: "center",
            padding: "16px",
            width: "400px",
            height: "266px",
          }}>
          <img
            src={`data:image/jpeg;base64,${courseDataById?.image}`}
            style={{
              width: "100%",
              height: "100%",
              objectFit: "cover",
            }}
          />
        </Box>

        <Box
          sx={{
            padding: "16px",
            width: "700px",
            height: "266px",
          }}>
          <Typography variant='p' color='text.secondary'>
            {courseDataById?.category}
          </Typography>

          <Typography variant='h6' component='div' fontWeight='bold'>
            {courseDataById?.title}
          </Typography>

          <Typography
            variant='h6'
            component='div'
            sx={{ fontWeight: "bold" }}
            className='text-cornflowerBlue'
            marginTop={3}>
            {courseDataById?.price && priceIDFormat(courseDataById.price)}
          </Typography>

          <Autocomplete
            size='small'
            value={selectedOption}
            onChange={(event, newValue) => {
              setSelectedOption(newValue);
              const selectedId = newValue ? newValue.id : null;
              setSelectedId(selectedId);
            }}
            inputValue={inputValue}
            onInputChange={(event, newInputValue) => {
              setInputValue(newInputValue);
            }}
            id='controllable-states-demo'
            options={options}
            getOptionLabel={(option) => option?.date}
            sx={{
              width: 300,
              marginTop: 2,
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label='Pilih Jadwal Kelas'
                inputProps={{ ...params.inputProps, readOnly: "readonly" }}
              />
            )}
          />

          <Box>
            <Button
              sx={{
                borderColor: "hsl(239, 61%, 65%)",
                border: 1,
                paddingX: 2,
                paddingY: 0.5,
                width: 200,
                marginTop: 3,
                "&:hover": {
                  backgroundColor: "hsl(239, 61%, 65%)",
                  color: "white",
                },
              }}
              onClick={handleCart}>
              {"Masukan Keranjang"}
            </Button>

            <Button
              sx={{
                backgroundColor: "hsl(239, 61%, 65%)",
                color: "white",
                border: 1,
                paddingX: 2,
                paddingY: 0.5,
                width: 200,
                marginTop: 3,
                marginLeft: { xs: 0, md: 1 },
                "&:hover": {
                  backgroundColor: "hsl(239, 61%, 75%)",
                  border: 1,
                  borderColor: "hsl(239, 61%, 65%)",
                  color: "hsl(239, 61%, 45%)",
                },
              }}
              onClick={handleBuy}>
              Beli Sekarang
            </Button>
          </Box>
        </Box>

        <Box
          sx={{
            padding: { xs: "16px", md: "2px" },
            width: { xs: "100%", md: 700, lg: 1150 },
            marginTop: { xs: "20px", md: "0px" },
          }}>
          <Typography
            variant='h5'
            sx={{
              textAlign: "justify",
              fontWeight: "bold",
              marginBottom: "1rem",
            }}>
            Deskripsi
          </Typography>
          <Typography
            sx={{
              textAlign: "left",
              fontSize: "1.15rem",
            }}>
            {courseDataById?.description}
          </Typography>
        </Box>
      </Grid>

      <Grid
        sx={{
          display: "flex",
          gridGap: "16px",
          padding: 1,
          justifyContent: "center",
          alignItems: "center",
          paddingX: 5,
        }}>
        <Box
          display='flex'
          justifyContent='center'
          alignItems='center'
          flexDirection={"column"}
          flexGrow={1}>
          <Typography
            variant='h5'
            marginBottom={3}
            marginTop={8}
            color={"hsl(239, 61%, 65%)"}
            fontWeight='bold'>
            Kelas lain yang mungkin kamu suka
          </Typography>
          <Grid
            container
            width={"80%"}
            display='flex'
            justifyContent='center'
            alignItems='center'
            sx={{
              width: {
                md: "100vw",
                lg: "80vw",
                xl: "70vw",
              },
            }}>
            {activecourseData?.map((courseItem, index) => (
              <FavClassCard
                key={index}
                items={{
                  id: courseItem.id,
                  source: courseItem.image,
                  type: courseItem.category,
                  name: courseItem.title,
                  price: courseItem.price,
                }}
              />
            ))}
          </Grid>
        </Box>
      </Grid>

      <Footer />
    </div>
  );
};

export default DetailClass;
