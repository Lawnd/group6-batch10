
import Footer from "../../components/Footer";
import { NavLink, useNavigate } from "react-router-dom";
import InvoiceTableCard from "../../components/InvoiceTableCard";
import {
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Grid,
    Button,
    Typography,
    Breadcrumbs,
    Link,
  } from "@mui/material";
import useAuth from "../../hooks/useAuth";
import axios from "axios";
import { useEffect, useState } from "react";


const Invoice = () => {
     const navigate = useNavigate();
     const apiUrl = process.env.REACT_APP_API_URL;
     const { payload } = useAuth(); 
     const [invoiceData, setInvoiceData] = useState([]);


     const options = {
        headers: {
          Authorization: `Bearer ${payload}`,
          'Content-Type': 'application/json',
        },
      };

     const getInvoiceData = async () => {
        try {
          if (!payload) {
            navigate("/login");
            return;
          }
    
          const response = await axios.get(`${apiUrl}Invoice/GetInvoiceByIdUser`, options);
    
          if (response.status === 200) {
            const invoiceData = response.data; 
            setInvoiceData(invoiceData);
          } else {
          }
        } catch (error) {
        }
      };

      useEffect(() => {
        getInvoiceData();
      }, [payload]);
    
    return(
        <div>
            <Grid container 
            sx={{
                display: 'flex',
                gridGap: '16px', 
                padding: 1,
                justifyContent: 'center', 
                alignItems: 'center', 
                paddingX: 5,
                marginBottom: 20,
                
              
              }}
            >
            
            <Box 
            sx={{     
            width: {xs:"300px", sm:"800px", md:"1000px", lg:"1500px"},
            marginTop: 2
            }}>
            
            <Breadcrumbs separator="›" aria-label="breadcrumb">
                <Typography fontWeight='bold'> 
                    <Link underline="hover" key="1" color="inherit"
                    component={NavLink}
                    to="/"
                    >
                    Beranda
                    </Link>
                </Typography>

                <Typography key="3" color={"hsl(239, 61%, 65%)"} fontWeight='bold'>
                Invoice
                </Typography>
            </Breadcrumbs>
        

            <Typography
            variant='h6'
            component='div'
            fontWeight='bold'
            marginTop={5}
            >
            Menu Invoice
            </Typography>

            </Box>


            <Box 
            sx={{ width: { xs:"300px", sm:"800px", md:"1000px", lg:"1500px"}}} > 
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                <TableHead style={{ backgroundColor: "#F2C94C", color: "white" }}>
                    <TableRow>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>No</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>No. Invoice</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Tanggal Beli</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Jumlah Kursus</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Total Harga</TableCell>
                    <TableCell align="center" sx={{ fontWeight: "bold" }}>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {invoiceData?.map((item, index) => (
                <InvoiceTableCard
                    key={item.orderId} 
                    item={{
                    id: item.orderId, 
                    no: index + 1,
                    invoice: item.noInvoice,
                    date: item.created,
                    totalcourse: item.totalCourse,
                    price: item.totalCost,
                    }}
                    index={index}
                    navigate={navigate}
                />
                ))}

                </TableBody>
                </Table>
            </TableContainer>
            </Box>

            </Grid>

            <Footer />
        </div>
   );
}

export default Invoice;