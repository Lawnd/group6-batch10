import { NavLink } from "react-router-dom";
import React from "react";
import { Container, Button, Box, Typography } from "@mui/material";
import success from "../../assets/images/email-succes.svg";

const EmailConfirmPass = () => {
    
    
    return (
     <>
        <Container component="main" 
        sx={{ mb: 5, mt: 5,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"}}  >

            <Box
                sx={{
                    height: {
                    xs: "15rem",
                    md: "18rem",
                    lg: "20rem",
                },
                }}
                component='img'
                src={success}
            />
            
            <Typography
            sx={{
              fontSize: "1.55rem",
              fontWeight: "bold",
              marginBottom: "1.35rem",
              color: "hsl(239, 61%, 65%)",
            }}>
            Reset Password Telah Berhasil
            </Typography>
            <Typography
                sx={{
                textAlign: "center",
                fontSize: "1.15rem",
                }}>
                Silahkan Login terlebih dahulu untuk masuk ke aplikasi
            </Typography>

            <NavLink to='/login'>
            <Button
                sx={{
                backgroundColor: "hsl(239, 61%, 65%)",
                color: "white",
                border: 1,
                paddingX: 3,
                paddingY: 1,
                marginTop: 1,
                width: "100%",
                minWidth: 115,

                "&:hover": {
                    backgroundColor: "hsl(239, 61%, 75%)",
                    border: 1,
                    borderColor: "hsl(239, 61%, 65%)",
                    color: "hsl(239, 61%, 45%)",
                },
                }}>
                Masuk Sekarang
            </Button>
            </NavLink>
        </Container>
     </>
    ) 
}

export default EmailConfirmPass;