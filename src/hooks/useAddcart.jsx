import { useState } from "react";

export default function useAddcart() {
  const [isAdded, setIsAdded] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  //const apiUrl = process.env.REACT_APP_API_URL;

  const add = () => {
    if (selectedOption) {
      console.log("Mengirim opsi yang dipilih ke API:", selectedOption);
      setIsAdded(true);

    //   try {
    //     // Mengirim data ke API dengan metode POST
    //     const response = await axios.post(`${apiUrl}/order`, {
    //       option: selectedOption,
    //     });

    //     if (response.status === 200) {
    //       console.log("Berhasil menambahkan ke keranjang:", response.data);
    //       setIsAdded(true);
    //     } else {
    //       console.error("Gagal menambahkan ke keranjang:", response.data);
    //     }
    //   } catch (error) {
    //     console.error("Terjadi kesalahan:", error);
    //   }

    } else {
      alert("Pilih jadwal terlebih dahulu");
    }
  };

  return {
    isAdded,
    selectedOption,
    setSelectedOption,
    add,
  };
}
