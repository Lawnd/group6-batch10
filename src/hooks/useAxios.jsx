import { useState } from "react";
import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

const useAxios = ({ url, method, body = null, headers = null }) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const fetchData = () => {
    setLoading(true);
    axios({
      method,
      url,
      headers,
      data: body,
    })
      .then((res) => {
        setResponse(res.data);
      })
      .catch((err) => {
        if (err.response) setError(err.response.data);
        if (err.request)
          setError("Network error. Please check your internet connection.");
        setError("An error occurred. Please try again later.");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return { response, error, loading, fetchData };
};

export default useAxios;
