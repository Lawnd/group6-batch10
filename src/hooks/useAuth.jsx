import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

export default function useAuth() {
  const { payload, userData, login, logout } = useContext(AuthContext);

  return {
    payload,
    userData,
    login,
    logout,
  };
}
