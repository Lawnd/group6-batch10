import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import { AuthProvider } from "./context/AuthContext";
//Page-List
import { Header } from "./components";
import {
  ForgotPassword,
  LoginPage,
  ResetPassword,
  RegisterPage,
  EmailConfirm,
  EmailCheck,
  EmailConfirmPass,
  DetailClass,
  CheckoutPage,
  LandingPage,
  MenuClassPage,
  SuccessPurchase,
  Invoice,
  MyClass,
  PaymentMethodPage,
  DetailInvoice,
} from "./pages";

import useAuth from "./hooks/useAuth";
import {
  DashboardAdmin,
  ManageCategory,
  ManageUser,
  ManagePayment,
  ManageInvoice,
  ManageCourse,
  DetailInvoiceAdmin,
} from "./admin/pages";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<Header />}>
      <Route index element={<LandingPage />} />
      <Route path='*' element={<LandingPage />} />
      <Route path='checkoutpage' element={<CheckoutPage />} />
      <Route path='menuclass/:id' element={<MenuClassPage />} />
      <Route path='detailclass/:id' element={<DetailClass />} />
      <Route path='successpurchase' element={<SuccessPurchase />} />
      <Route path='invoice' element={<Invoice />} />
      <Route path='detailinvoice/:id' element={<DetailInvoice />} />
      <Route path='myclass' element={<MyClass />} />
      <Route path='paymentmethod' element={<PaymentMethodPage />} />
    </Route>
  )
);

const routerWithoutAcc = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<Header />}>
      <Route index element={<LandingPage />} />
      <Route path='*' element={<LandingPage />} />
      <Route path='login' element={<LoginPage />} />
      <Route path='register' element={<RegisterPage />} />
      <Route path='forgotpass' element={<ForgotPassword />} />
      <Route path='resetpass' element={<ResetPassword />} />
      <Route path='emailconfirm' element={<EmailConfirm />} />
      <Route path='emailnotification' element={<EmailCheck />} />
      <Route path='emailpass' element={<EmailConfirmPass />} />
      <Route path='menuclass/:id' element={<MenuClassPage />} />
      <Route path='detailclass/:id' element={<DetailClass />} />
    </Route>
  )
);

const adminRouter = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path='/' element={<Header />}>
        <Route index element={<LandingPage />} />
        <Route path='*' element={<LandingPage />} />
        <Route path='checkoutpage' element={<CheckoutPage />} />
        <Route path='menuclass/:id' element={<MenuClassPage />} />
        <Route path='detailclass/:id' element={<DetailClass />} />
        <Route path='successpurchase' element={<SuccessPurchase />} />
        <Route path='invoice' element={<Invoice />} />
        <Route path='detailinvoice/:id' element={<DetailInvoice />} />
        <Route path='myclass' element={<MyClass />} />
        <Route path='paymentmethod' element={<PaymentMethodPage />} />
      </Route>
      <Route path='/dashboard' element={<DashboardAdmin />}>
        <Route index element={<ManageUser />} />
        <Route path='managecourse' element={<ManageCourse />} />
        <Route path='manageuser' element={<ManageUser />} />
        <Route path='managecategory' element={<ManageCategory />} />
        <Route path='managepayment' element={<ManagePayment />} />
        <Route path='manageinvoice' element={<ManageInvoice />} />
        <Route path='manageinvoice/:id' element={<DetailInvoiceAdmin />} />
      </Route>
    </>
  )
);

const RouterApp = () => {
  const { userData } = useAuth();

  if (userData?.role == "True") return <RouterProvider router={adminRouter} />;
  if (userData?.email) return <RouterProvider router={router} />;

  return <RouterProvider router={routerWithoutAcc} />;
};

export default RouterApp;
