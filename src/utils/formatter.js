const priceIDFormat = (num) => {
  if (!num) return 0;
  const price = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return `IDR ${price}`;
};

const daysOfWeek = [
  "Minggu",
  "Senin",
  "Selasa",
  "Rabu",
  "Kamis",
  "Jum'at",
  "Sabtu",
];

const monthsOfYear = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

const dateFormat = (dateString) => {
  if (!dateString) {
    const dateObject = new Date();
  }
  const dateObject = new Date(dateString);
  const year = dateObject.getFullYear();
  const month = dateObject.getMonth();
  const day = dateObject.getDay();
  const dateOfMonth = dateObject.getDate();

  const formattedDay = daysOfWeek[day];
  const formattedMonth = monthsOfYear[month];

  const formattedDate = `${formattedDay}, ${dateOfMonth} ${formattedMonth} ${year}`;

  return formattedDate;
};

export { priceIDFormat, dateFormat };
