const sum = (cartCheckout) => {
  const itemSelected = cartCheckout.filter(
    (value) => value.isSelected === true
  );
  return itemSelected.reduce((a, b) => a + b.price, 0);
};

export { sum };
