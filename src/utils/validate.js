const validatePassword = (string) => {
  const regularExpression =
    /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  return regularExpression.test(string);
};

const validateEmail = (string) => {
  const regExp =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  return regExp.test(string);
};

const validateName = (string) => {
  const regExp = /^([\w]{3,})+\s+([\w\s]{3,})+$/i;
  return regExp.test(string);
};

const isMatch = (string1, string2) => {
  return string1 === string2;
};

export { validateEmail, validatePassword, validateName, isMatch };
