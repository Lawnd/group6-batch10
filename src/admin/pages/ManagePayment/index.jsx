import {
  Box,
  Button,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import AddPayment from "./AddPayment";
import axios from "axios";
import EditPayment from "./EditPayment";
import TableItemCard from "./TableItemCard";

const ManagePayment = () => {
  const [modalAdd, setModalAdd] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [selectedEdit, setSelectedEdit] = useState(null);
  const [payments, setPayments] = useState([]);
  const [isError, setError] = useState(null);

  const [isLoading, setLoading] = useState(false);

  async function getPayments() {
    setPayments([]);
    try {
      setLoading(true);
      const response = await axios.get("/Payment");
      setPayments(response.data);

      setError(null);
    } catch (error) {
      setError(error); // Set the error state on failure
    } finally {
      setLoading(false);
    }
  }

  const handleModalAdd = () => {
    setModalAdd(!modalAdd);
  };

  const handleModalEdit = (v) => {
    setModalEdit(!modalEdit);
    setSelectedEdit(v);
  };

  const handleSuccess = async () => {
    window.location.reload();
  };

  useEffect(() => {
    getPayments();
  }, []);

  if (isLoading) return <CircularProgress />;

  return (
    <>
      <Box
        sx={{
          margin: {
            xs: "0.5rem",
            md: "3rem",
          },
        }}>
        <Typography
          sx={{
            fontSize: { xs: "1rem", md: "1.5rem" },
            fontWeight: "bold",
          }}>
          Dashboard Payment Method
        </Typography>
        <Box>
          <Box
            sx={{
              width: "100%",
              height: "3rem",
              display: "flex",
              justifyContent: "end",
            }}>
            <Button
              onClick={handleModalAdd}
              sx={{
                width: {
                  xs: "10rem",
                  md: "15rem",
                },
                backgroundColor: "#5D5FEF",
                marginX: {
                  xs: "0",
                  md: "1rem",
                },
                color: "white",
                ":hover": {
                  bgcolor: "#989af4", // theme.palette.primary.main
                  color: "white",
                },
              }}>
              Tambah Payment
            </Button>
          </Box>

          <Box
            sx={{
              overflowX: "scroll",
            }}>
            <Table
              sx={{
                marginTop: "1rem",
                overflowX: "scroll",
              }}>
              <TableHead>
                <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Image</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Method</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Status</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Created</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Actions</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {payments?.map((v) => {
                  return (
                    <TableItemCard
                      key={v?.id}
                      item={v}
                      handleSuccess={handleSuccess}
                      handleModalEdit={handleModalEdit}
                    />
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Box>
      </Box>
      {modalAdd && (
        <AddPayment
          onClose={handleModalAdd}
          open={modalAdd}
          handleClose={handleModalAdd}
          handleAddSuccess={handleSuccess}
        />
      )}

      {modalEdit && (
        <EditPayment
          item={selectedEdit}
          onClose={handleModalEdit}
          open={true}
          handleClose={handleModalEdit}
          handleEditSuccess={handleSuccess}
        />
      )}
    </>
  );
};

export default ManagePayment;
