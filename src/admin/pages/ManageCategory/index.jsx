import {
  Box,
  Button,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import TableItemCard from "./TableItemCard";
import EditCategory from "./EditCategory";
import AddCategory from "./AddCategory";
import "../../../styles/custom.css";

const ManageCategory = () => {
  const [modalAdd, setModalAdd] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [selectedEdit, setSelectedEdit] = useState(null);
  const [categories, setCategories] = useState([]);
  const [isError, setError] = useState(null);

  const [isLoading, setLoading] = useState(false);

  async function getCategories() {
    setCategories([]);
    try {
      setLoading(true);
      const response = await axios.get("/CategoryCourse");
      setCategories(response.data);

      setError(null);
    } catch (error) {
      setError(error); // Set the error state on failure
    } finally {
      setLoading(false);
    }
  }

  const handleModalAdd = () => {
    setModalAdd(!modalAdd);
  };

  const handleModalEdit = (v) => {
    setModalEdit(!modalEdit);
    setSelectedEdit(v);
  };

  const handleSuccess = async () => {
    window.location.reload();
    // await getCategories();
  };

  useEffect(() => {
    getCategories();
  }, []);

  if (isLoading) return <CircularProgress />;
  // if (isError) return <Typography>Error: {isError.message}</Typography>;

  return (
    <>
      <Box
        sx={{
          margin: {
            sm: "0.5rem",
            lg: "2rem",
            xl: "3rem",
          },
          overflowX: "hidden",
        }}>
        <Typography
          sx={{
            fontSize: { xs: "1rem", md: "1.5rem" },
            fontWeight: "bold",
          }}>
          Dashboard Category Course
        </Typography>
        <Box>
          <Box
            sx={{
              width: "100%",
              height: "3rem",
              display: "flex",
              justifyContent: "end",
              marginTop: "0.5rem",
            }}>
            <Button
              onClick={handleModalAdd}
              sx={{
                width: {
                  xs: "60%",
                  sm: "40%",
                  md: "30%",
                  xl: "15%",
                },
                backgroundColor: "#5D5FEF",
                marginX: {
                  sm: "0.2rem",
                  lg: "1rem",
                },
                color: "white",
                ":hover": {
                  bgcolor: "#989af4", // theme.palette.primary.main
                  color: "white",
                },
              }}>
              Tambah Category
            </Button>
          </Box>

          <Box
            sx={{
              overflowX: "scroll",
            }}>
            <Table
              sx={{
                marginTop: "1rem",
                overflowX: "scroll",
              }}>
              <TableHead sx={{ width: "1rem", overflowX: "scroll" }}>
                <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Image</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Name</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Description</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Status</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Actions</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody sx={{ width: "1rem" }}>
                {categories?.map((v) => {
                  return (
                    <TableItemCard
                      key={v?.id}
                      item={v}
                      handleSuccess={handleSuccess}
                      handleModalEdit={handleModalEdit}
                      api='CategoryCourse'
                    />
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Box>
      </Box>
      {modalAdd && (
        <AddCategory
          onClose={handleModalAdd}
          open={modalAdd}
          handleClose={handleModalAdd}
          handleAddSuccess={handleSuccess}
        />
      )}

      {modalEdit && (
        <EditCategory
          item={selectedEdit}
          onClose={handleModalEdit}
          open={true}
          handleClose={handleModalEdit}
          handleEditSuccess={handleSuccess}
        />
      )}
    </>
  );
};

export default ManageCategory;
