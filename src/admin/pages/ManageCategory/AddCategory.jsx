import { useState } from "react";
import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import { AiOutlinePlusSquare } from "react-icons/ai";
import "../../../styles/custom.css";
import { useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const AddCategory = (props) => {
  const [data, setData] = useState({
    Name: "",
    Description: "",
    ImageFile: null,
  });
  const [selectedFile, setSelectedFile] = useState(null);
  const [image, setImage] = useState(null);
  const [isActive, setActive] = useState(true);
  const formData = new FormData();

  const onFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    const reader = new FileReader();
    reader.onload = (e) => {
      setImage(e.target.result);
    };
    reader.readAsDataURL(file);
  };

  const handleChangeDescription = (event) => {
    setData({
      ...data,
      Description: event.target.value,
    });
  };

  const handleChangeName = (event) => {
    setData({
      ...data,
      Name: event.target.value,
    });
  };

  const handleChangeActive = () => {
    setActive(!isActive);
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/CategoryCourse`,
    method: "POST",
    body: formData,
    headers: {
      "content-type": "multipart/form-data",
    },
  });

  const handleInsert = async () => {
    formData.append("ImageFile", selectedFile);
    formData.append("Name", data.Name);
    formData.append("Description", data.Description);
    formData.append("IsActive", isActive);

    try {
      await fetchData();
      await props?.handleAddSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            md: "30rem",
          },
          borderRadius: "1rem",
          padding: "1.5rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Add Category Course
        </Typography>
        {image ? (
          <>
            <Box
              component='img'
              sx={{
                height: "15rem",
                width: "15rem",
              }}
              className='photo-modal'
              src={image}
            />
            <label htmlFor='upload-photo'>Change Image...</label>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
          </>
        ) : (
          <label htmlFor='upload-photo'>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
            <Box
              sx={{
                border: "0.2rem dotted",
                width: "10rem",
                height: "10rem",
                marginX: "auto",
                marginY: "1rem",
                borderRadius: "1rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                "&:hover": {
                  cursor: "pointer",
                  transform: "scale(1.05)",
                },
              }}>
              <AiOutlinePlusSquare size={50} />
            </Box>
          </label>
        )}

        <TextField
          label={"Category Course"}
          onChange={(e) => handleChangeName(e)}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <TextField
          label={"Description"}
          onChange={(e) => handleChangeDescription(e)}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton handleChange={handleChangeActive} checked={isActive} />
        </Box>

        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default AddCategory;
