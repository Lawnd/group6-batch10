import { useState } from "react";
import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import { AiOutlinePlusSquare } from "react-icons/ai";
import { useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const EditCategory = (props) => {
  const [data, setData] = useState({
    Name: props?.item?.name || "",
    Description: props?.item?.description || "",
    ImageFile: `data:image/jpeg;base64,${props?.item?.image}`,
    IsActive: props?.item?.isActive,
  });
  const [selectedFile, setSelectedFile] = useState(props?.item?.image);
  const [image, setImage] = useState(props?.item?.image);
  const formData = new FormData();

  const onFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    const readerr = new FileReader();
    readerr.onload = (e) => {
      setImage(e.target.result);
      setData((prevState) => ({
        ...prevState,
        ImageFile: e.target.result,
      }));
    };
    readerr.readAsDataURL(file);
  };

  const handleChange = (event, field) => {
    if (field === "IsActive") {
      setData({
        ...data,
        IsActive: event?.target?.checked ? true : false,
      });
      return;
    }

    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `CategoryCourse/Update?id=${props?.item?.id}`,
    method: "PUT",
    body: formData,
    headers: {
      "content-type": "multipart/form-data",
    },
  });

  const handleInsert = async () => {
    formData.append("ImageFile", selectedFile);
    formData.append("Description", data.Description);
    formData.append("IsActive", data.IsActive);
    formData.append("Name", data.Name);

    try {
      await fetchData();
      await props?.handleEditSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      key={props?.id}
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        key={props?.id}
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            md: "30rem",
          },
          borderRadius: "1rem",
          padding: "1.5rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Edit Category Course
        </Typography>
        {image ? (
          <>
            <Box component='img' className='photo-modal' src={data.ImageFile} />
            <label htmlFor='update-photo'>Change Image...</label>
            <input
              type='file'
              name='photo'
              id='update-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
          </>
        ) : (
          <label htmlFor='update-photo'>
            <input
              type='file'
              name='photo'
              id='update-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
            <Box
              sx={{
                border: "0.2rem dotted",
                width: "10rem",
                height: "10rem",
                marginX: "auto",
                marginY: "1rem",
                borderRadius: "1rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                "&:hover": {
                  cursor: "pointer",
                  transform: "scale(1.05)",
                },
              }}>
              <AiOutlinePlusSquare size={50} />
            </Box>
          </label>
        )}

        <TextField
          id={props?.id}
          label={"Category Course"}
          value={data.Name}
          onChange={(e) => handleChange(e, "Name")}
          sx={{ width: "90%", marginY: "1rem", marginX: "auto" }}
        />

        <TextField
          id={props?.id}
          label={"Description"}
          value={data.Description}
          onChange={(e) => handleChange(e, "Description")}
          sx={{ width: "90%", marginY: "1rem", marginX: "auto" }}
        />

        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton
            handleChange={(e) => handleChange(e, "IsActive")}
            checked={data.IsActive}
          />
        </Box>

        <Button
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            color: "white",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default EditCategory;
