import DashboardAdmin from "./DashboardAdmin";
import ManageUser from "./ManageUser";
import ManageCourse from "./ManageCourse";
import ManageCategory from "./ManageCategory";
import ManagePayment from "./ManagePayment";
import ManageInvoice from "./ManageInvoice";
import DetailInvoiceAdmin from "./DetailInvoiceAdmin";

export {
  DashboardAdmin,
  ManageUser,
  ManageCourse,
  ManageCategory,
  ManagePayment,
  ManageInvoice,
  DetailInvoiceAdmin,
};
