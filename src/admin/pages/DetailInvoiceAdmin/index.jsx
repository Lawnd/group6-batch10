import {
  Box,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import TableItemCard from "./TableItemCard";
import { useNavigate, useParams } from "react-router-dom";
import { dateFormat, priceIDFormat } from "../../../utils/formatter";

const DetailInvoiceAdmin = (props) => {
  const [invoice, setInvoice] = useState([]);
  const [orderInvoice, setOrderInvoice] = useState([]);
  const [isError, setError] = useState(null);
  let navigate = useNavigate();

  const [isLoading, setLoading] = useState(false);

  const { id } = useParams();

  async function getInvoices() {
    setInvoice([]);
    try {
      setLoading(true);
      const response = await axios.get(`Invoice/GetInvoiceById?id_order=${id}`);
      setInvoice(response.data);
      setError(null);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  }

  async function getOrderInvoice() {
    setOrderInvoice([]);
    try {
      setLoading(true);
      const response = await axios.get(
        `Invoice/GetInvoiceDetail?id_order=${id}`
      );
      setOrderInvoice(response.data);
      setError(null);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getInvoices();
    getOrderInvoice();
  }, []);

  return (
    <Box
      sx={{
        margin: {
          xs: "0.5rem",
          md: "3rem",
        },
      }}>
      <Button
        onClick={() => navigate("/dashboard/manageinvoice")}
        sx={{
          backgroundColor: "#5D5FEF",
          marginBottom: {
            xs: "0.5rem",
            md: "1rem",
          },
          color: "white",
          ":hover": {
            bgcolor: "#989af4",
            color: "white",
          },
        }}>
        Back
      </Button>
      <Typography
        sx={{
          fontSize: { xs: "1rem", md: "1.5rem" },
        }}>
        Detail Invoice
      </Typography>
      <Box sx={{ fontWeight: "bold" }}>{invoice?.noInvoice}</Box>
      <Typography
        sx={{
          fontSize: { xs: "1rem", md: "1.2rem", color: "#404040" },
        }}>
        Jumlah Course : {invoice?.totalCourse}
      </Typography>
      <Typography
        sx={{
          fontSize: { xs: "1rem", md: "1.2rem", color: "#404040" },
        }}>
        Harga : {priceIDFormat(invoice?.totalCost)}
      </Typography>
      <Typography
        sx={{
          fontSize: { xs: "1rem", md: "1.2rem", color: "#404040" },
        }}>
        Tanggal : {dateFormat(invoice?.created)}
      </Typography>
      <Box
        sx={{
          overflowX: "scroll",
        }}>
        <Table
          sx={{
            marginTop: "1rem",
            overflowX: "scroll",
          }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
              <TableCell>
                <Typography fontWeight={"bold"}>Category</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Title</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Date</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Price</Typography>
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {orderInvoice?.map((v, i) => {
              return <TableItemCard key={i} item={v} />;
            })}
          </TableBody>
        </Table>
      </Box>
    </Box>
  );
};

export default DetailInvoiceAdmin;
