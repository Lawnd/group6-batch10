import { TableCell, TableRow, Typography } from "@mui/material";
import "../../../styles/custom.css";
import { dateFormat, priceIDFormat } from "../../../utils/formatter";

const TableItemCard = (props) => {
  return (
    <TableRow className='table-item-card'>
      <TableCell>
        <Typography>{props?.item?.category}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.title}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{dateFormat(props?.item?.date)}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{priceIDFormat(props?.item?.price)}</Typography>
      </TableCell>
    </TableRow>
  );
};

export default TableItemCard;
