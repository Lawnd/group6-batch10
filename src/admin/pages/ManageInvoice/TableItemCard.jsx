import { Button, TableCell, TableRow, Typography } from "@mui/material";
import "../../../styles/custom.css";
import { dateFormat, priceIDFormat } from "../../../utils/formatter";

const TableItemCard = (props) => {
  return (
    <TableRow key={props?.item?.orderId} className='table-item-card'>
      <TableCell>
        <Typography>{props?.item?.noInvoice}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.email}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.totalCourse}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{priceIDFormat(props?.item?.totalCost)}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{dateFormat(props?.item?.created)}</Typography>
      </TableCell>
      <TableCell>
        <Button
          variant='contained'
          onClick={() => props?.navigateDetailInvoices(props?.item?.orderId)}>
          Detail
        </Button>
      </TableCell>
    </TableRow>
  );
};

export default TableItemCard;
