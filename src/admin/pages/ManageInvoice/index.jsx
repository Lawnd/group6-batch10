import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import TableItemCard from "./TableItemCard";
import { useNavigate } from "react-router-dom";

const ManageInvoice = () => {
  const [invoices, setInvoices] = useState([]);
  const [isError, setError] = useState(null);
  const navigate = useNavigate();

  const [isLoading, setLoading] = useState(false);

  async function getPayments() {
    setInvoices([]);
    try {
      setLoading(true);
      const response = await axios.get("Invoice/GetAllInvoice");

      setInvoices(response.data);
      setError(null);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getPayments();
  }, []);

  const navigateDetailInvoices = (id) => {
    navigate(`/dashboard/manageinvoice/${id}`);
  };

  // if (isLoading) return <CircularProgress />;
  // if (isError) return <Typography>Error: {isError.message}</Typography>;

  return (
    <Box
      sx={{
        margin: {
          xs: 0,
          md: "3rem",
        },
      }}>
      <Typography
        sx={{
          fontSize: { xs: "1rem", md: "1.5rem" },
          fontWeight: "bold",
        }}>
        Dashboard Invoice
      </Typography>
      <Box
        sx={{
          overflowX: "scroll",
        }}>
        <Table
          sx={{
            marginTop: "1rem",
            overflowX: "scroll",
          }}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
              <TableCell>
                <Typography fontWeight={"bold"}>NoInvoice</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Email</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Total Course</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Total Price</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Tanggal</Typography>
              </TableCell>
              <TableCell>
                <Typography fontWeight={"bold"}>Actions</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoices?.map((v, id) => {
              return (
                <TableItemCard
                  key={id}
                  item={v}
                  navigateDetailInvoices={navigateDetailInvoices}
                />
              );
            })}
          </TableBody>
        </Table>
      </Box>
    </Box>
  );
};

export default ManageInvoice;
