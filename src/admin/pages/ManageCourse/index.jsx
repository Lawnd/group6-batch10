import {
  Box,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import TableItemCard from "./TableItemCard";
import AddCourse from "./AddCourse";
import EditCourse from "./EditCourse";
import AddSchedule from "./AddSchedule";

const ManageCourse = () => {
  const [modal, setModal] = useState({
    addCourse: false,
    editCourse: false,
    addSchedule: false,
  });
  const [selectedEdit, setSelectedEdit] = useState(null);
  const [course, setCourse] = useState({
    response: [],
    error: "",
    loading: false,
  });
  const [categories, setCategories] = useState({
    response: [],
    error: "",
    loading: false,
  });

  async function fetchData(endpoint, setState) {
    setState((prevState) => ({
      ...prevState,
      response: [],
      loading: true,
      error: null,
    }));

    try {
      const response = await axios.get(endpoint);
      setState((prevState) => ({
        ...prevState,
        response: response.data,
        error: null,
      }));
    } catch (error) {
      setState((prevState) => ({
        ...prevState,
        error: error,
      }));
    } finally {
      setState((prevState) => ({
        ...prevState,
        loading: false,
      }));
    }
  }

  const handleModal = (field) => {
    setModal((prevState) => ({
      ...prevState,
      [field]: !prevState?.[field],
    }));
  };

  async function getCourse(setCourse) {
    await fetchData("/Course", setCourse);
  }

  async function getCategory(setCategories) {
    await fetchData("/CategoryCourse", setCategories);
  }

  const handleModalEdit = (v) => {
    setSelectedEdit(v);
    setModal((prevState) => ({
      ...prevState,
      editCourse: !prevState?.editCourse,
    }));
  };

  const handleSuccess = async () => {
    window.location.reload();
  };

  useEffect(() => {
    getCategory(setCategories);
    getCourse(setCourse);
  }, []);

  return (
    <>
      <Box
        sx={{
          margin: {
            sm: "0.5rem",
            lg: "2rem",
            xl: "3rem",
          },
          overflowX: "hidden",
        }}>
        <Typography
          sx={{
            fontSize: { xs: "1rem", md: "1.5rem" },
            fontWeight: "bold",
            marginBottom: "0.5rem",
          }}>
          Dashboard Course
        </Typography>
        <Box>
          <Box
            sx={{
              width: "100%",
              height: "3rem",
              display: "flex",
              justifyContent: "end",
            }}>
            <Button
              onClick={() => handleModal("addSchedule")}
              sx={{
                width: {
                  xs: "35%",
                  md: "15%",
                },
                backgroundColor: "#5D5FEF",
                marginX: "1rem",
                color: "white",
                ":hover": {
                  bgcolor: "#989af4", // theme.palette.primary.main
                  color: "white",
                },
              }}>
              Tambah Schedule
            </Button>
            <Button
              onClick={() => handleModal("addCourse")}
              sx={{
                width: {
                  xs: "35%",
                  md: "15%",
                },
                backgroundColor: "#5D5FEF",
                marginX: {
                  xs: "0",
                  md: "1rem",
                },
                color: "white",
                ":hover": {
                  bgcolor: "#989af4", // theme.palette.primary.main
                  color: "white",
                },
              }}>
              Tambah Course
            </Button>
          </Box>

          <Box
            sx={{
              overflowX: "scroll",
            }}>
            <Table
              sx={{
                marginTop: "1rem",
                overflowX: "scroll",
              }}>
              <TableHead>
                <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Image</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Title</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Description</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Price</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Status</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Updated</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Created</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Actions</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {course?.response?.map((v) => {
                  return (
                    <TableItemCard
                      key={v?.id}
                      item={v}
                      handleModalEdit={handleModalEdit}
                      api='CategoryCourse'
                    />
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Box>
      </Box>
      {modal.addCourse && (
        <AddCourse
          onClose={() => handleModal("addCourse")}
          open={modal.addCourse}
          handleClose={() => handleModal("addCourse")}
          handleSuccess={handleSuccess}
          categories={categories?.response}
        />
      )}

      {modal.addSchedule && (
        <AddSchedule
          onClose={() => handleModal("addSchedule")}
          open={modal.addSchedule}
          handleClose={() => handleModal("addSchedule")}
          handleSuccess={handleSuccess}
          course={course?.response}
        />
      )}

      {modal.editCourse && (
        <EditCourse
          item={selectedEdit}
          onClose={() => handleModal("editCourse")}
          open={true}
          handleClose={() => handleModal("editCourse")}
          handleSuccess={handleSuccess}
          categories={categories?.response}
        />
      )}
    </>
  );
};

export default ManageCourse;
