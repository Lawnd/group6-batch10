import React, { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { AiOutlinePlusSquare } from "react-icons/ai";
import "../../../styles/custom.css";
import { useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const AddCourse = (props) => {
  const [isActive, setActive] = useState(true);
  const [selectedFile, setSelectedFile] = useState(null);
  const [image, setImage] = useState(null);

  const formData = new FormData();

  const [data, setData] = useState({
    Title: "",
    Description: "",
    ImageFile: "",
    Price: 0,
    IsActive: true,
    CategoryId: "",
  });

  const onFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    const reader = new FileReader();
    reader.onload = (e) => {
      setImage(e.target.result);
    };
    reader.readAsDataURL(file);
  };

  const handleChange = (event, field) => {
    if (field === "IsActive") {
      setActive(!isActive);
      setData({
        ...data,
        IsActive: event?.target?.checked ? true : false,
      });
      return;
    }

    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/Course`,
    method: "POST",
    body: formData,
    headers: {
      "content-type": "multipart/form-data",
    },
  });

  const handleInsert = async () => {
    formData.append("Title", data.Title);
    formData.append("Description", data.Description);
    formData.append("Price", data.Price);
    formData.append("IsActive", data.IsActive);
    formData.append("CategoryId", data.CategoryId);
    formData.append("ImageFile", selectedFile);

    try {
      await fetchData();
      await props?.handleSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            md: "30rem",
          },
          borderRadius: "1rem",
          padding: "2rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Add Course
        </Typography>
        {image ? (
          <>
            <Box
              className='photo-modal'
              component='img'
              sx={{
                height: "15rem",
                width: "15rem",
              }}
              src={image}
            />
            <label htmlFor='upload-photo'>Change Image...</label>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
          </>
        ) : (
          <label htmlFor='upload-photo'>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
            <Box
              sx={{
                border: "0.2rem dotted",
                width: "10rem",
                height: "10rem",
                marginX: "auto",
                marginY: "1rem",
                borderRadius: "1rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                "&:hover": {
                  cursor: "pointer",
                  transform: "scale(1.05)",
                },
              }}>
              <AiOutlinePlusSquare size={50} />
            </Box>
          </label>
        )}
        <TextField
          label={"Title"}
          onChange={(e) => handleChange(e, "Title")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <TextField
          label={"Description"}
          onChange={(e) => handleChange(e, "Description")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <TextField
          label={"Price"}
          onChange={(e) => handleChange(e, "Price")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <FormControl sx={{ m: 1, width: "90%" }}>
          <InputLabel id='demo-simple-select-helper-label'>Category</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={data.CategoryId}
            label='Category'
            onChange={(e) => handleChange(e, "CategoryId")}
            sx={{ width: "100%" }}>
            {props?.categories?.map((v) => {
              return (
                <MenuItem key={v.id} value={v.id}>
                  {v.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton
            handleChange={(e) => handleChange(e, "IsActive")}
            checked={isActive}
          />
        </Box>

        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default AddCourse;
