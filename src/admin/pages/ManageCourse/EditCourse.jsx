import { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { AiOutlinePlusSquare } from "react-icons/ai";
import "../../../styles/custom.css";
import { useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const EditCourse = (props) => {
  const [selectedFile, setSelectedFile] = useState(props?.item?.image);
  const [image, setImage] = useState(props?.item?.image);

  const formData = new FormData();
  const matchingCategory = props?.categories.find(
    (category) => category.name === props?.item?.category
  );

  const [data, setData] = useState({
    Title: props?.item?.title,
    Description: props?.item?.description,
    ImageFile: `data:image/jpeg;base64,${props?.item?.image}`,
    Price: props?.item?.price,
    IsActive: props?.item?.isActive,
    CategoryId: matchingCategory?.id,
  });

  const onFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    const reader = new FileReader();
    reader.onload = (e) => {
      setImage(e.target.result);
      setData((prevState) => ({
        ...prevState,
        ImageFile: e.target.result,
      }));
    };
    reader.readAsDataURL(file);
  };

  const handleChange = (event, field) => {
    if (field === "IsActive") {
      setData({
        ...data,
        IsActive: event?.target?.checked ? true : false,
      });
      return;
    }

    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/Course/${props?.item?.id}`,
    method: "PUT",
    body: formData,
    headers: {
      "content-type": "multipart/form-data",
    },
  });

  const handleInsert = async () => {
    formData.append("Title", data.Title);
    formData.append("Description", data.Description);
    formData.append("Price", data.Price);
    formData.append("IsActive", data.IsActive);
    formData.append("CategoryId", data.CategoryId);
    formData.append("ImageFile", selectedFile);

    try {
      await fetchData();
      await props?.handleSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            md: "30rem",
          },
          borderRadius: "1rem",
          padding: "2rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Add Course
        </Typography>
        {image ? (
          <>
            <img src={data.ImageFile} className='photo-modal' />
            <label htmlFor='upload-photo'>Change Image...</label>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
          </>
        ) : (
          <label htmlFor='upload-photo'>
            <input
              type='file'
              name='photo'
              id='upload-photo'
              onChange={onFileChange}
              style={{ opacity: 0, position: "absolute", zIndex: -1 }}
            />
            <Box
              sx={{
                border: "0.2rem dotted",
                width: "10rem",
                height: "10rem",
                marginX: "auto",
                marginY: "1rem",
                borderRadius: "1rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                "&:hover": {
                  cursor: "pointer",
                  transform: "scale(1.05)",
                },
              }}>
              <AiOutlinePlusSquare size={50} />
            </Box>
          </label>
        )}
        <TextField
          label={"Title"}
          value={data.Title}
          onChange={(e) => handleChange(e, "Title")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <TextField
          label={"Description"}
          value={data.Description}
          onChange={(e) => handleChange(e, "Description")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <TextField
          label={"Price"}
          value={data.Price}
          onChange={(e) => handleChange(e, "Price")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <FormControl sx={{ m: 1, width: "90%" }}>
          <InputLabel id='demo-simple-select-helper-label'>Category</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            label='Category'
            value={data.CategoryId}
            onChange={(e) => handleChange(e, "CategoryId")}
            sx={{ width: "100%" }}>
            {props?.categories?.map((v) => {
              return (
                <MenuItem key={v.id} value={v.id}>
                  {v.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton
            handleChange={(e) => handleChange(e, "IsActive")}
            checked={data.IsActive}
          />
        </Box>

        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default EditCourse;
