import React, { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  Typography,
} from "@mui/material";
import "../../../styles/custom.css";
import { useAxios } from "../../../hooks";
import { DatePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";

const AddSchedule = (props) => {
  const [isActive, setActive] = useState(true);
  const [role, setRole] = useState("Admin");
  const [data, setData] = useState({
    Date: new Date(),
    CourseId: "",
  });

  const onDatePicked = (event) => {
    setData((prevData) => ({
      ...prevData,
      Date: event?.$d,
    }));
  };

  const handleChange = (event, field) => {
    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/Schedule`,
    method: "POST",
    body: data,
  });

  const handleInsert = async () => {
    try {
      await fetchData();
      await props?.handleSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            md: "30rem",
          },
          borderRadius: "1rem",
          padding: "2rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Add Schedule
        </Typography>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            sx={{ width: "90%", marginTop: "1rem" }}
            value={dayjs(data?.Date)}
            inputFormat='E MMM dd yyyy HH:MM:SS O'
            onChange={(e) => onDatePicked(e)}
          />
        </LocalizationProvider>

        <FormControl sx={{ m: 1, width: "90%" }}>
          <InputLabel id='demo-simple-select-helper-label'>Course</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={data.CourseId}
            label='Course'
            onChange={(e) => handleChange(e, "CourseId")}
            sx={{ width: "100%" }}>
            {props?.course?.map((v) => {
              return (
                <MenuItem key={v.id} value={v.id}>
                  {v.title}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default AddSchedule;
