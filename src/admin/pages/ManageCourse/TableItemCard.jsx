import { Box, Button, TableCell, TableRow, Typography } from "@mui/material";
import { AiFillEdit } from "react-icons/ai";
import "../../../styles/custom.css";
import { DeleteButton } from "../../components";
import { dateFormat, priceIDFormat } from "../../../utils/formatter";

const TableItemCard = (props) => {
  return (
    <TableRow key={props?.id} className='table-item-card'>
      <TableCell>
        <img
          src={`data:image/jpeg;base64,${props?.item?.image}`}
          className='photo'
        />
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.title}</Typography>
      </TableCell>
      <TableCell>
        <Typography>
          {props?.item?.description?.length <= 10
            ? props?.item?.description
            : props?.item?.description?.substr(0, 18) + "..."}
        </Typography>
      </TableCell>
      <TableCell>
        <Typography>{priceIDFormat(props?.item.price)}</Typography>
      </TableCell>
      <TableCell>
        <Box
          sx={{
            width: "3.5rem",
            height: "2.5rem",
            display: "flex",
            backgroundColor: props?.item?.isActive ? "#7CFC00" : "#ff0000",
            borderRadius: "0.5rem",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Typography
            fontWeight='bold'
            sx={{ color: props?.item?.isActive ? "black" : "white" }}>
            {props?.item?.isActive ? "ON" : "OFF"}
          </Typography>
        </Box>
      </TableCell>
      <TableCell>
        <Typography>{dateFormat(props?.item.updated)}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{dateFormat(props?.item.created)}</Typography>
      </TableCell>
      <TableCell>
        <Button onClick={() => props?.handleModalEdit(props?.item)}>
          <AiFillEdit size={20} />
        </Button>
        <DeleteButton id={props?.item?.id} api={`Course/`} />
      </TableCell>
    </TableRow>
  );
};

export default TableItemCard;
