import {
  Box,
  Button,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import TableItemCard from "./TableItemCard";
import EditUser from "./EditUser";
import AddUser from "./AddUser";
import { useAuth } from "../../../hooks";

const ManageUser = () => {
  const [modalAdd, setModalAdd] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);
  const [selectedEdit, setSelectedEdit] = useState(null);
  const [users, setUsers] = useState([]);
  const [isError, setError] = useState(null);

  const [isLoading, setLoading] = useState(false);

  const { payload } = useAuth();

  const config = {
    headers: {
      Authorization: `Bearer ${payload}`,
    },
  };

  async function getUsers() {
    setUsers([]);
    try {
      setLoading(true);
      const response = await axios.get("User/GetAllUsers", config);
      setUsers(response.data);
      setError(null);
    } catch (error) {
      setError(error); // Set the error state on failure
    } finally {
      setLoading(false);
    }
  }

  const handleModalAdd = () => {
    setModalAdd(!modalAdd);
  };

  const handleModalEdit = (v) => {
    setModalEdit(!modalEdit);
    setSelectedEdit(v);
  };

  const handleSuccess = async () => {
    window.location.reload();
    // await getCategories();
  };

  useEffect(() => {
    getUsers();
  }, []);

  if (isLoading) return <CircularProgress />;
  // if (isError) return <Typography>Error: {isError.message}</Typography>;

  return (
    <>
      <Box
        sx={{
          margin: {
            sm: "0.5rem",
            lg: "2rem",
            xl: "3rem",
          },
          overflowX: "hidden",
        }}>
        <Typography
          sx={{
            fontSize: { xs: "1rem", md: "1.5rem" },
            fontWeight: "bold",
          }}>
          Dashboard Account
        </Typography>
        <Box>
          <Box
            sx={{
              width: "100%",
              height: "3rem",
              display: "flex",
              justifyContent: "end",
            }}>
            <Button
              onClick={handleModalAdd}
              sx={{
                width: {
                  xs: "40%",
                  sm: "30%",
                  md: "20%",
                },
                backgroundColor: "#5D5FEF",
                marginX: "1rem",
                color: "white",
                ":hover": {
                  bgcolor: "#989af4", // theme.palette.primary.main
                  color: "white",
                },
              }}>
              Tambah Account
            </Button>
          </Box>

          <Box
            sx={{
              overflowX: "scroll",
              marginTop: "1rem",
            }}>
            <Table
              sx={{
                marginTop: "1rem",
                overflowX: "scroll",
              }}>
              <TableHead>
                <TableRow sx={{ backgroundColor: "#FFFBF1" }}>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Full Name</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Role</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Email</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Status</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography fontWeight={"bold"}>Actions</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {users?.map((v, id) => {
                  return (
                    <TableItemCard
                      key={id}
                      item={v}
                      handleSuccess={handleSuccess}
                      handleModalEdit={handleModalEdit}
                      api='CategoryCourse'
                    />
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </Box>
      </Box>
      {modalAdd && (
        <AddUser
          onClose={handleModalAdd}
          open={modalAdd}
          handleClose={handleModalAdd}
          handleAddSuccess={handleSuccess}
        />
      )}

      {modalEdit && (
        <EditUser
          item={selectedEdit}
          onClose={handleModalEdit}
          open={true}
          handleClose={handleModalEdit}
          handleEditSuccess={handleSuccess}
        />
      )}
    </>
  );
};

export default ManageUser;
