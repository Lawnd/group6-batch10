import React, { useState } from "react";
import {
  Box,
  Button,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import "../../../styles/custom.css";
import { useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const AddUser = (props) => {
  const [isActive, setActive] = useState(true);
  const [role, setRole] = useState("Admin");
  const [data, setData] = useState({
    Fullname: "",
    Password: "Password123!",
    Email: "",
    Role: true,
    IsActivated: true,
  });

  const handleChange = (event, field) => {
    if (field === "IsActivated") {
      setActive(!isActive);
      setData({
        ...data,
        IsActivated: event?.target?.checked ? true : false,
      });
      return;
    }
    if (field === "Role") {
      setData({
        ...data,
        Role: event?.target?.value == "Admin" ? true : false,
      });
      setRole(event?.target?.value);
      return;
    }

    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/User/Register`,
    method: "POST",
    body: data,
  });

  const handleInsert = async () => {
    try {
      await fetchData();
      await props?.handleAddSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: {
            xs: "22rem",
            sm: "25rem",
            lg: "30rem",
          },
          borderRadius: "1rem",
          padding: "1.5rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Add Account
        </Typography>
        <Box sx={{ width: "90%", marginTop: "1.5rem" }}>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={role}
            label='Role'
            onChange={(e) => handleChange(e, "Role")}>
            <MenuItem value={"Admin"}>Admin</MenuItem>
            <MenuItem value={"User"}>User</MenuItem>
          </Select>
        </Box>

        <TextField
          label={"Full Name"}
          onChange={(e) => handleChange(e, "Fullname")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <TextField
          label={"Email"}
          onChange={(e) => handleChange(e, "Email")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />
        <TextField
          label={"Password"}
          value={data?.Password}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton
            handleChange={(e) => handleChange(e, "IsActivated")}
            checked={isActive}
          />
        </Box>

        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default AddUser;
