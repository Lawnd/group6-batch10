import { Box, Button, TableCell, TableRow, Typography } from "@mui/material";
import { AiFillEdit } from "react-icons/ai";
import "../../../styles/custom.css";

const TableItemCard = (props) => {
  return (
    <TableRow key={props?.id} className='table-item-card'>
      <TableCell>
        <Typography>{props?.item?.fullname}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.role ? "Admin" : "User"}</Typography>
      </TableCell>
      <TableCell>
        <Typography>{props?.item?.email}</Typography>
      </TableCell>

      <TableCell>
        <Box
          sx={{
            width: "3.5rem",
            height: "2.5rem",
            display: "flex",
            backgroundColor: props?.item?.isActivated ? "#7CFC00" : "#ff0000",
            borderRadius: "0.5rem",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <Typography
            fontWeight='bold'
            sx={{ color: props?.item?.isActivated ? "black" : "white" }}>
            {props?.item?.isActivated ? "ON" : "OFF"}
          </Typography>
        </Box>
      </TableCell>
      <TableCell>
        <Button onClick={() => props?.handleModalEdit(props?.item)}>
          <AiFillEdit size={20} />
        </Button>
      </TableCell>
    </TableRow>
  );
};

export default TableItemCard;
