import React, { useState } from "react";
import {
  Box,
  Button,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import "../../../styles/custom.css";
import { useAuth, useAxios } from "../../../hooks";
import { ToggleButton } from "../../components";

const EditUser = (props) => {
  const [data, setData] = useState({
    Fullname: props?.item?.fullname,
    Email: props?.item?.email,
    Role: props?.item?.role,
    IsActivated: props?.item?.isActivated,
  });

  const { payload } = useAuth();

  const handleChange = (event, field) => {
    if (field === "IsActivated") {
      setData({
        ...data,
        IsActivated: event?.target?.checked ? true : false,
      });
      return;
    }
    if (field === "Role") {
      setData({
        ...data,
        Role: event?.target?.value == "Admin" ? true : false,
      });
      return;
    }

    setData({
      ...data,
      [field]: event.target.value,
    });
  };

  const { response, error, loading, fetchData } = useAxios({
    url: `/User/Update?userId=${props?.item?.id}`,
    method: "PUT",
    body: data,
    headers: {
      Authorization: `Bearer ${payload}`,
    },
  });

  const handleInsert = async () => {
    try {
      await fetchData();
      await props?.handleEditSuccess();
      await props?.handleClose();
    } catch (e) {
      throw e;
    }
  };

  return (
    <Modal
      open={props?.open}
      onClose={props?.handleClose}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Box
        sx={{
          backgroundColor: "white",
          width: "30rem",
          borderRadius: "1rem",
          padding: "1.5rem",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Typography
          id='modal-modal-title'
          variant='h6'
          component='h2'
          fontWeight='bold'>
          Edit Account
        </Typography>
        <Box sx={{ width: "90%", marginTop: "1.5rem" }}>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={data?.Role ? "Admin" : "User"}
            label='Role'
            onChange={(e) => handleChange(e, "Role")}>
            <MenuItem value={"Admin"}>Admin</MenuItem>
            <MenuItem value={"User"}>User</MenuItem>
          </Select>
        </Box>

        <TextField
          label={"Full Name"}
          value={data?.Fullname}
          onChange={(e) => handleChange(e, "Fullname")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <TextField
          label={"Email"}
          value={data?.Email}
          onChange={(e) => handleChange(e, "Email")}
          sx={{ width: "90%", marginY: "0.7rem", marginX: "auto" }}
        />

        <Box sx={{ marginY: "1rem" }}>
          <Typography sx={{ textAlign: "center", paddingBottom: "0.3rem" }}>
            Status
          </Typography>
          <ToggleButton
            handleChange={(e) => handleChange(e, "IsActivated")}
            checked={data.IsActivated}
          />
        </Box>

        <Button
          variant='contained'
          onClick={handleInsert}
          sx={{
            marginY: "1rem",
            backgroundColor: "#5D5FEF",
            "&:hover": {
              backgroundColor: "#989af4",
            },
          }}>
          <Typography>Insert</Typography>
        </Button>
      </Box>
    </Modal>
  );
};

export default EditUser;
