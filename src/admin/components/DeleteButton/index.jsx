import { Button, CircularProgress } from "@mui/material";
import { useAxios } from "../../../hooks";
import { AiFillDelete } from "react-icons/ai";

const DeleteButton = (props) => {
  const { loading, fetchData } = useAxios({
    url: `/${props?.api}${props?.id}`,
    method: "DELETE",
  });

  const handleDeleteClick = async () => {
    try {
      await fetchData();
      await window.location.reload();
    } catch (error) {
      alert(error);
    }
  };

  return (
    <>
      <Button onClick={handleDeleteClick}>
        <AiFillDelete size={20} color='#FF0000' />
      </Button>
      {loading && <CircularProgress />}
    </>
  );
};

export default DeleteButton;
