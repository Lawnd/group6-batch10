import {
  Button,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from "@mui/material";
import {
  Apple as AppleIcon,
  PersonSearch as PersonSearchIcon,
  QueueMusic as QueueMusicIcon,
  MusicNote as MusicNoteIcon,
  AddCard as AddCardIcon,
  Receipt as ReceiptIcon,
} from "@mui/icons-material";
import { useAuth } from "../../../hooks";

const SideNavbarItems = ({ navigate }) => {
  const { userData, logout } = useAuth();

  const menuItems = [
    {
      text: "User",
      icon: <PersonSearchIcon />,
      onClick: () => navigate("manageuser"),
    },
    {
      text: "Course",
      icon: <MusicNoteIcon />,
      onClick: () => navigate("managecourse"),
    },
    {
      text: "Category",
      icon: <QueueMusicIcon />,
      onClick: () => navigate("managecategory"),
    },
    {
      text: "Payment Method",
      icon: <AddCardIcon />,
      onClick: () => navigate("managepayment"),
    },
    {
      text: "Invoice",
      icon: <ReceiptIcon />,
      onClick: () => navigate("manageinvoice"),
    },
  ];

  const handleLogout = () => {
    try {
      logout();
      navigate("/");
    } catch (e) {
      throw e;
    }
  };

  return (
    <div>
      <Toolbar
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "start",
          mb: 4,
          mt: 2,
        }}>
        <IconButton sx={{ marginBottom: "1rem" }} onClick={() => navigate("/")}>
          <AppleIcon sx={{ color: "#000000", width: "3rem", height: "3rem" }} />
          <Typography sx={{ marginLeft: "0.5rem" }}>Apple Music</Typography>
        </IconButton>
        <Typography sx={{ flexGrow: 1, textAlign: "center" }}>
          Welcome <span style={{ fontWeight: "bold" }}> {userData?.email}</span>
        </Typography>
      </Toolbar>

      <List>
        {menuItems.map((item, index) => (
          <ListItem
            key={index}
            disablePadding
            sx={{ "&:hover": { backgroundColor: "rgba(250, 188, 29, 0.2)" } }}>
            <ListItemButton onClick={item.onClick}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>

      <Button
        onClick={handleLogout}
        sx={{
          position: "absolute",
          bottom: 0,
          mb: 3,
          ml: 6,
          px: 4,
          borderRadius: 2,
          backgroundColor: "#ff0000",
        }}
        variant='contained'>
        Logout
      </Button>
    </div>
  );
};

export default SideNavbarItems;
