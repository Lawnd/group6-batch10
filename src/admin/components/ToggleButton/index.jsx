import { Stack, Switch, Typography, styled } from "@mui/material";

const ToggleButton = (props) => {
  const AntSwitch = styled(Switch)(({ theme }) => ({
    width: 48,
    height: 32,
    padding: 0,
    borderRadius: 50,
    display: "flex",
    "&:active": {
      "& .MuiSwitch-thumb": {
        width: 15,
      },
      "& .MuiSwitch-switchBase.Mui-checked": {
        transform: "translateX(9px)",
      },
    },
    "& .MuiSwitch-switchBase": {
      padding: 3,
      "&.Mui-checked": {
        transform: "translateX(16px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          opacity: 1,
          backgroundColor:
            theme.palette.mode === "dark" ? "#177ddc" : "#1890ff",
        },
      },
    },
    "& .MuiSwitch-thumb": {
      boxShadow: "0 2px 4px 0 rgb(0 35 11 / 20%)",
      width: 26,
      height: 26,
      borderRadius: 100,
      transition: theme.transitions.create(["width"], {
        duration: 200,
      }),
    },
    "& .MuiSwitch-track": {
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor:
        theme.palette.mode === "dark"
          ? "rgba(255,255,255,.35)"
          : "rgba(0,0,0,.25)",
      boxSizing: "border-box",
    },
  }));
  return (
    <Stack direction='row' spacing={1} alignItems='center'>
      <Typography>Off</Typography>
      <AntSwitch
        checked={props?.checked}
        onChange={(e) => props?.handleChange(e)}
        inputProps={{ "aria-label": "ant design" }}
      />
      <Typography>On</Typography>
    </Stack>
  );
};

export default ToggleButton;
