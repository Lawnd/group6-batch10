import React from "react";
import "./styles/output.css";
import { AuthProvider } from "./context/AuthContext";
import RouterApp from "./RouteApp";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

export default function App() {
  return (
    <AuthProvider>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <RouterApp />
      </LocalizationProvider>
    </AuthProvider>
  );
}
